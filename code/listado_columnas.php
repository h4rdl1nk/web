<?php

require_once(realpath('controlv2/config/db.php'));

$db_schema = $config['db']['v2']['dbname'];
$db_user = $config['db']['v2']['users']['rw']['username'];
$db_pass = $config['db']['v2']['users']['rw']['password'];
$db_host = $config['db']['v2']['host'];

$conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
if ($mysqli->connect_errno) {
    echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}


$page = $_GET['id'];
$table = "autocaravanas_stock";
$imgTable = $table . '_imagen';
$marca = $_GET['marca'];
$imagesDir =  $config['images']['rootDir'] . '/' . $config['images'][$table]['rootDir'];

$query_autocaravanas = str_replace('?',$marca,$config['db']['v2']['tables'][$table]['detail_query_marca']);

if ($res_autocaravanas = $conn->query($query_autocaravanas)) {
    if ($res_autocaravanas->num_rows > 0){
        while($row_autocaravana = mysqli_fetch_array($res_autocaravanas, MYSQLI_ASSOC)){

            $query_images = "SELECT t.nombre AS tipo,i.nombre AS nombre 
                           FROM " . $imgTable . " i 
                           JOIN imagen_tipo t 
                           WHERE i.id_tipo = t.id
                           AND i.id_element = '" . $row_autocaravana['id'] . "' 
                           AND t.nombre IN ('principal','planta');";
                           
            if ($res_images = $conn->query($query_images)) {
                while($row_image = mysqli_fetch_array($res_autocaravanas, MYSQLI_ASSOC)){
                    if($row_image['tipo' ] == "principal"){
                        $img_left = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/' . $row_image['nombre'];
                    }
                    if($row_image['tipo' ] == "planta"){
                        $img_bottom = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/' . $row_image['nombre'];
                    }
                }
            }

            $logoMarca = '/' . $config['images']['rootDir'] . '/' . $config['images']['autocaravanas_marca']['rootDir'] . '/' . $row_autocaravana['id_marca'] . '/logo.jpeg';
            $title_str = ucfirst($row_autocaravana['marca']) . " " . ucfirst($row_autocaravana['gama']) . " " . $row_autocaravana['nombre'] . " (" . $row_autocaravana['tipo'] . ")";
            //$img_left = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/principal.jpeg';
            //$img_bottom = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/planta.jpeg';
            if( $page != "ocasion" && $page != "alquiler-autocaravanas"){
                $img_right = $logoMarca;
            }
            else{
                $img_right = "/img/logo_nusa.png";
            }
            ?>
                <a href="/<?php echo $_GET['id']; ?>/<?php echo $row_autocaravana['marca']; ?>/<?php echo $row_autocaravana['tipo']; ?>/<?php echo $row_autocaravana['nombre']; ?>">
                <div id="c_elem_detail">
                    <div id="elem_detail">
                        <div id="elem_detail_title">
                            <?php echo $title_str; ?>
                        </div>
                        <div id="elem_detail_img_left">
                            <img style="width:100%;" src="<?php echo $img_left; ?>"></img>
                        </div>
                        <div id="elem_detail_img_right">
                            <img style="width:100%;" src="<?php echo $img_right; ?>"></img>
                        </div>
                        <div id="elem_detail_img_bottom">
                            <img style="width:100%;" src="<?php echo $img_bottom; ?>"></img>
                        </div>
                    </div>
                </div>
                </a>
            <?php
        }
    }
}

$conn->close();