<?php

require_once(realpath('code/include/functions.php'));

?>

<!--INICIO contenedor_seccion-->
<div class="contenedor_seccion">

<?php

//Localizacion
$page = $_GET['id'];

if( $page == "autocaravanas" && isset($_GET['stock']) && !isset($_GET['marca']) && !isset($_GET['modelo']) ){

    ?>
    <!--TITULO SECCION-->   
    <div class="titulo_seccion">
        <div style="float:left;margin-left:5px;">
            <font>
                <a href="/autocaravanas" rel="nofollow"><h1>Autocaravanas</h1></a> > <h2>STOCK</h2>
            </font>
        </div>
        <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
            <a href="/<?php echo $_GET['id']; ?>" rel="nofollow">
                <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
                <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" alt="Flecha atras" style="max-height:25px;"></img></div>
            </a>
        </div>
    </div>
    <?php

    listadoAutocaravanasMarca($page,'autocaravanas_stock','all');

}
elseif( $page == "autocaravanas" && isset($_GET['stock']) && isset($_GET['marca']) && isset($_GET['modelo'])){

    $marca = $_GET['marca'];
    $modeltemp = $_GET['modelo'];
    $modeltemparr = explode('-',$modeltemp);
    $modelo = "";

    for ($i = 0; $i < count($modeltemparr); $i++) {
        switch(true){
            case ( $i == 0 ):
                $id = $modeltemparr[$i];
                break;
            case ( $i < ( count($modeltemparr) - 1 ) && $i > 0 ):
                $modelo = $modelo . ' ' . ucfirst($modeltemparr[$i]);
                break;
            case ( $i == ( count($modeltemparr) - 1 ) ):
                $temporada = $modeltemparr[$i];
                break;
        }
    }

    ?>
    <!--TITULO SECCION-->
    <div class="titulo_seccion">
        <div style="float:left;margin-left:5px;">
            <font>
                <a href="/<?php echo $page; ?>" rel="nofollow"><h1>Autocaravanas</h1></a> > <h2><a href="/<?php echo $page; ?>/stock">STOCK</a></h2> > <h3><?php echo strtoupper($marca); ?>  <?php echo $modelo; ?> (<?php echo $temporada; ?>)</h3>
            </font>
        </div>
        <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
            <a href="/<?php echo $page; ?>/stock" rel="nofollow">
                <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
                <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" alt="Flecha atras" style="max-height:25px;"></img></div>
            </a>
        </div>
    </div>
    <?php

    detalleAutocaravana($page,'autocaravanas_stock',$id);

}
elseif( $page == "autocaravanas" && !isset($_GET['marca']) ){

    ?>
    <div class="titulo_seccion"><h1>Autocaravanas</h1></div>

    <div class="texto_centrado">
      Somos distribuidores de autocaravanas Giottiline, PLA, Weinsberg y Pilote.
      <br>Compra tu autocaravana nueva con nosotros y p&aacute;gala en c&oacute;modos plazos, ya que ofrecemos unas magn&iacute;ficas condiciones de financiaci&oacute;n. Ven a visitarnos y comprueba la calidad de nuestros veh&iacute;culos en nuestras instalaciones.
      <br>Ense&ntilde;amos a nuestros clientes todo lo necesario para aprovechar las capacidades de su nueva autocaravana, ¡aprende con nosotros y disfruta de tu viaje!
    </div>

    <div class="btn_marca_autocaravana">
    <a href="/autocaravanas/stock">
        <img src="/img/boton_stock.gif" alt="Autocaravanas en stock"></img>
    </a>
    </div>

   <div id="separador_linea">MARCAS, CATALOGOS Y DISTRIBUCIONES</div>


    <?php

    listadoMarcas($page,'autocaravanas_marca');

}
elseif ( $page == "autocaravanas" && isset($_GET['marca']) && !isset($_GET['gama']) ){
    
    $marca = $_GET['marca'];

    ?>
    <!--TITULO SECCION-->
    <div class="titulo_seccion">
        <div style="float:left;margin-left:5px;">
            <font>
                <a href="/autocaravanas" rel="nofollow"><h1>Autocaravanas</h1></a> > <h2><?php echo strtoupper($_GET['marca']); ?></h2>
            </font>
        </div>
        <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
            <a href="/<?php echo $_GET['id']; ?>" rel="nofollow">
                <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
                <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" alt="Flecha atras" style="max-height:25px;"></img></div>
            </a>
        </div>
    </div>
    <?php

    listadoAutocaravanasGama($page,'autocaravanas_catalogo',$marca);
    
}
elseif ( $page == "autocaravanas" && isset($_GET['marca']) && isset($_GET['gama']) && !isset($_GET['modelo'])){
    
    $marca = $_GET['marca'];
    $gama = $_GET['gama'];
    $gamaStr = strtoupper(implode(' ',explode('-',$gama)));

    ?>
    <!--TITULO SECCION-->
    <div class="titulo_seccion">
        <div style="float:left;margin-left:5px;">
            <font>
                <a href="/<?php echo $page; ?>" rel="nofollow"><h1>Autocaravanas</h1></a> > <a href="/<?php echo $page; ?>/<?php echo $marca; ?>"><h2><?php echo ucfirst($marca); ?></h2></a> > <h3><?php echo $gamaStr; ?></h3>
            </font>
        </div>
        <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
            <a href="/<?php echo $page; ?>/<?php echo $marca; ?>" rel="nofollow">
                <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
                <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" alt="Flecha atras" style="max-height:25px;"></img></div>
            </a>
        </div>
    </div>
    <?php

    listadoModelosGama($page,'autocaravanas_catalogo',$marca,$gama);

}
elseif ( $page == "autocaravanas" && isset($_GET['marca']) && isset($_GET['gama']) && isset($_GET['modelo'])){

    $marca = $_GET['marca'];
    $gama = $_GET['gama'];
    $gamaStr = strtoupper(implode(' ',explode('-',$gama)));
    $modeltemp = $_GET['modelo'];
    $modeltemparr = explode('-',$modeltemp);
    $modelo = "";

    for ($i = 0; $i < count($modeltemparr); $i++) {
        switch(true){
            case ( $i == 0 ):
                $id = $modeltemparr[$i];
                break;
            case ( $i < ( count($modeltemparr) - 1 ) && $i > 0 ):
                $modelo = $modelo . ' ' . ucfirst($modeltemparr[$i]);
                break;
            case ( $i == ( count($modeltemparr) - 1 ) ):
                $temporada = $modeltemparr[$i];
                break;
        }
    }

    ?>
    <!--TITULO SECCION-->
    <div class="titulo_seccion">
        <div style="float:left;margin-left:5px;">
            <font>
                <a href="/<?php echo $page; ?>" rel="nofollow"><h1>Autocaravanas</h1></a> > <a href="/<?php echo $page; ?>/<?php echo $marca; ?>"><h2><?php echo ucfirst($marca); ?></h2></a> > <a href="/<?php echo $page; ?>/<?php echo $marca; ?>/<?php echo $gama; ?>"><h3><?php echo $gamaStr; ?></h3></a> > <h4><?php echo $modelo; ?> (<?php echo $temporada; ?>)</h4>
            </font>
        </div>
        <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
            <a href="/<?php echo $page; ?>/<?php echo $marca; ?>/<?php echo $gama; ?>" rel="nofollow">
                <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
                <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" alt="Flecha atras" style="max-height:25px;"></img></div>
            </a>
        </div>
    </div>
    <?php

    detalleAutocaravana($page,'autocaravanas_catalogo',$id);

}
else {
    echo 'ERROR';
}

?>
</div>
<!--FIN contenedor_seccion-->
