<?php

require_once(realpath('controlv2/config/db.php'));

$db_schema = $config['db']['v2']['dbname'];
$db_user = $config['db']['v2']['users']['rw']['username'];
$db_pass = $config['db']['v2']['users']['rw']['password'];
$db_host = $config['db']['v2']['host'];

$conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
if ($mysqli->connect_errno) {
    echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}


$query_marcas = $config['db']['v2']['tables']['autocaravanas_marca']['list_query'];

if ($res_marcas = $conn->query($query_marcas)) {
    if ($res_marcas->num_rows > 0){
        while($row_marca = mysqli_fetch_array($res_marcas, MYSQLI_ASSOC)){
            $dirImagesMarca = $config['images']['rootDir'] . '/' . $config['images']['autocaravanas_marca']['rootDir'] . '/' . $row_marca['id'];
            ?>
            <div class="btn_marca_autocaravana">
            <a href="/autocaravanas/<?php echo $row_marca['nombre']; ?>">
                <img src="/<?php echo $dirImagesMarca; ?>/boton.jpeg" alt="Autocaravanas marca <?php echo $row_marca['nombre']; ?>"></img>
            </a>
            </div>
            <?php
        }
    }
}
else{
    echo 'No se han encontrado marcas';
}

$conn->close();

?>