<?php

function getDbConnection($mode){

    if ( require(realpath('controlv2/config/db.php')) ){

        $db_schema = $config['db']['v2']['dbname'];
        $db_user = $config['db']['v2']['users'][$mode]['username'];
        $db_pass = $config['db']['v2']['users'][$mode]['password'];
        $db_host = $config['db']['v2']['host'];

        $connection = new mysqli($db_host,$db_user,$db_pass,$db_schema);
        if ($connection->connect_errno) {
            return false;
        }
        else{
            return $connection;
        }

    }
    else{

        echo 'Error cargando variables';
        return false; 
    
    }

}

function getHeadInfo($item,$page,$table,$id){

    $str = "";

    require(realpath('controlv2/config/db.php'));

    $db_schema = $config['db']['v2']['dbname'];
    $db_user = $config['db']['v2']['users']['rw']['username'];
    $db_pass = $config['db']['v2']['users']['rw']['password'];
    $db_host = $config['db']['v2']['host'];

    $conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
    if ($conn->connect_errno) {
        echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    if($page == "autocaravanas" || $page == "alquiler-autocaravanas"){
        $query_modelo = "SELECT a.*,
                                m.id as id_marca,
                                g.id as id_gama,
                                m.nombre AS marca,
                                g.nombre AS gama,
                                t.nombre AS tipo
                        FROM " . $table . " a
                            JOIN autocaravanas_marca m
                            JOIN autocaravanas_gama g
                            JOIN autocaravanas_tipo t
                        WHERE a.id = '" . $id . "'
                            AND a.id_tipo = t.id
                            AND a.id_marca = m.id
                            AND a.id_gama = g.id";
        
        $title_vehicle = "autocaravana";
        $table_marca = "autocaravanas_marca";
        $table_marcaImg = "autocaravanas_marca_imagen";
    }
    elseif($page == "caravanas"){
        $query_modelo = "SELECT a.*,
                                m.id as id_marca,
                                g.id as id_gama,
                                m.nombre AS marca,
                                g.nombre AS gama
                        FROM " . $table . " a
                            JOIN caravanas_marca m
                            JOIN caravanas_gama g
                        WHERE a.id = '" . $id . "'
                            AND a.id_marca = m.id
                            AND a.id_gama = g.id";
        
        $title_vehicle = "caravana";
        $table_marca = "caravanas_marca";
        $table_marcaImg = "caravanas_marca_imagen";
    }elseif($page == "ocasion"){
        $query_modelo = "SELECT a.*
                         FROM " . $table . " a
                         WHERE a.id = '" . $id . "'";
        
        $title_vehicle = "ocasion";
    }

    if ($res_modelo = $conn->query($query_modelo)) {
        if ($res_modelo->num_rows > 0){
            $row_modelo = mysqli_fetch_array($res_modelo, MYSQLI_ASSOC);
        }
    }

    switch($item){
        case 'title':
            if($page == "ocasion"){
                $str = $row_modelo['nombre'] . " temporada " . $row_modelo['temporada'];
            }
            else{
                $str = ucfirst($title_vehicle) . " nueva " . ucfirst($row_modelo['marca']) . " " . $row_modelo['gama'] . " " . $row_modelo['nombre'] . " temporada " . $row_modelo['temporada'];
            }
            $res = $str;

            break;

        case 'image':
            $imagesDir =  $config['images']['rootDir'] . '/' . $config['images'][$table]['rootDir'] . '/' . $row_modelo['id'];
            $imgTable = $table . '_imagen';

            $query_list = "SELECT id,nombre 
                        FROM " . $imgTable . " 
                        WHERE id_element = '" . $row_modelo['id'] . "' AND id_tipo = 
                        (SELECT id FROM imagen_tipo WHERE nombre = 'galeria') ORDER BY LENGTH(nombre),nombre";

            if ($res_list = $conn->query($query_list)){
                $row_list = mysqli_fetch_array($res_list, MYSQLI_ASSOC);
                $imgSocial = '/' . $imagesDir . '/' . $row_list['nombre'];
            }

            $res = $imgSocial;

            break;
    }

    return $res;

    $conn->close();

}

function detalleAutocaravana($page,$table,$id){

    require(realpath('controlv2/config/db.php'));

    $db_schema = $config['db']['v2']['dbname'];
    $db_user = $config['db']['v2']['users']['rw']['username'];
    $db_pass = $config['db']['v2']['users']['rw']['password'];
    $db_host = $config['db']['v2']['host'];

    $conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
    if ($conn->connect_errno) {
        echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    if($page == "autocaravanas" || $page == "alquiler-autocaravanas"){
        $query_modelo = "SELECT a.*,
                                m.id as id_marca,
                                g.id as id_gama,
                                m.nombre AS marca,
                                g.nombre AS gama,
                                t.nombre AS tipo
                        FROM " . $table . " a
                            JOIN autocaravanas_marca m
                            JOIN autocaravanas_gama g
                            JOIN autocaravanas_tipo t
                        WHERE a.id = '" . $id . "'
                            AND a.id_tipo = t.id
                            AND a.id_marca = m.id
                            AND a.id_gama = g.id";
        
        $title_vehicle = "autocaravana";
        $table_marca = "autocaravanas_marca";
        $table_marcaImg = "autocaravanas_marca_imagen";
    }
    elseif($page == "caravanas"){
        $query_modelo = "SELECT a.*,
                                m.id as id_marca,
                                g.id as id_gama,
                                m.nombre AS marca,
                                g.nombre AS gama
                          FROM " . $table . " a
                            JOIN caravanas_marca m
                            JOIN caravanas_gama g
                          WHERE a.id = '" . $id . "'
                            AND a.id_marca = m.id
                            AND a.id_gama = g.id";
        
        $title_vehicle = "caravana";
        $table_marca = "caravanas_marca";
        $table_marcaImg = "caravanas_marca_imagen";
    }
    elseif($page == "ocasion"){
        $query_modelo = "SELECT a.*
                          FROM " . $table . " a
                          WHERE a.id = '" . $id . "'";
        
        $title_vehicle = "ocasion";
    }

    if ($res_modelo = $conn->query($query_modelo)) {
        if ($res_modelo->num_rows > 0){
            $row_modelo = mysqli_fetch_array($res_modelo, MYSQLI_ASSOC);

            if($page != "ocasion"){
                $query_marca_images = "SELECT t.nombre AS tipo,i.nombre AS nombre 
                                        FROM " . $table_marcaImg . " i 
                                        JOIN imagen_tipo t 
                                        WHERE i.id_tipo = t.id
                                        AND i.id_element = '" . $row_modelo['id_marca'] . "' 
                                        AND t.nombre IN ('logo');";

                if ($res_marca_images = $conn->query($query_marca_images)) {
                    while($row_marca_image = mysqli_fetch_array($res_marca_images, MYSQLI_ASSOC)){
                        if($row_marca_image['tipo' ] == "logo"){
                            $logoMarca = '/' . $config['images']['rootDir'] . '/' . $config['images'][$table_marca]['rootDir'] . '/' . $row_modelo['id_marca'] . '/' . $row_marca_image['nombre'];
                        }
                    }
                }
            }
            else{
                $logoMarca = '/' . $config['images']['rootDir'] . '/logo_nusa.png';
            }
        }
    }

    $imagesDir =  $config['images']['rootDir'] . '/' . $config['images'][$table]['rootDir'] . '/' . $row_modelo['id'];
    $imgTable = $table . '_imagen';

    $query_list = "SELECT id,nombre 
                   FROM " . $imgTable . " 
                   WHERE id_element = '" . $row_modelo['id'] . "' AND id_tipo = 
                   (SELECT id FROM imagen_tipo WHERE nombre = 'galeria') ORDER BY LENGTH(nombre),nombre";
    
    ?>
    <div class="fotorama"
         data-height="400" data-width="750" data-ratio="16/10"
         data-nav="thumbs" data-fit="contain" data-thumbfit="cover"
         data-autoplay="3000"
         data-arrows="true"
         data-click="true"
         data-swipe="true"
         data-allowfullscreen="native">


        <?php
            if ($res_list = $conn->query($query_list)){
                if ($res_list->num_rows > 0){
                    while ($row_list = mysqli_fetch_array($res_list, MYSQLI_ASSOC)){
                        ?>
                        <img src="/<?php echo $imagesDir . '/' . $row_list['nombre']; ?>" alt="<?php echo 'galeria ' . $title_vehicle . ' - ' . $row_modelo['marca'] . ' - ' . $row_modelo['tipo'] . ' - ' . $row_modelo['nombre'] . ' - ' . $row_list['id'];  ?> | Nusa Caravaning">
                        <?php
                    }
                }
                else{
                    ?>
                    <img src="/static/img/no_gallery.jpg" alt="No hay imagenes disponibles | Nusa Caravaning">
                    <?php
                }
            }
        ?>

    </div>

   <div id="contenedor_social">
           <div id="btn_social" class="fb-like" data-href="https://www.nusacaravaning.com<?php echo $_SERVER['REQUEST_URI']; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
           <div id="btn_social"><a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Mirad la <?php echo $title_vehicle; ?> <?php echo $row_modelo['marca'] . ' ' . $row_modelo['nombre'] . ' en Nusa Caravaning!!'; ?>">Tweet</a></div>
           <div id="btn_social"><div class="g-plusone" data-size="medium" data-annotation="inline" data-width="300"></div></div>
   </div>

   <div class="titulo_caracteristicas">
    <?php
        if($page != "ocasion"){
            echo ucfirst($title_vehicle) . ' nueva ' . strtoupper($row_modelo['marca']) . " " . strtoupper($row_modelo['gama']) . " " . strtoupper($row_modelo['nombre']);
        }
        else{
            echo ucfirst($title_vehicle) . ' ' . $row_modelo['nombre'];
        }
    ?>
   </div>

   <div class="cuadro_caracteristicas">

    <div class="enc_caracteristicas">
        <table width="100%">
         <tr>
           <td width="50%" style="text-align:left;">
              Marca: <?php echo strtoupper($row_modelo['marca']); ?> 
              <br>Gama: <?php echo strtoupper($row_modelo['gama']); ?>
              <br>Modelo: <?php echo strtoupper($row_modelo['nombre']); ?>
           </td>
           <td width="25%" style="text-align:right;">
             <a href="<?php echo $row['web']; ?>" target="_blank">
              <div style="width:100%;height:50px;">
               <img id="logo_caracteristicas" src="<?php echo $logoMarca; ?>" alt="Logo marca <?php echo $row_modelo['marca']?>"></img>
              </div>
             </a>
           </td>  
         </tr>
        </table>
      </div>

      <!--
      <?php
         if( $row['video'] != "" && $row['video'] != "-" && $row['video'] != "0" ){
           include 'code/box_video.php';
         }
      ?>
  -->

      <div id="box_caracteristicas_principales">
         <font class="bold_underline">CARACTERISITICAS PRINCIPALES</font>
         <br><br>
         <font><?php if($row_modelo['descripcion'] != ""){ echo nl2br($row_modelo['descripcion']) ;}else{ echo "Descripcion no disponible, contacte con nosotros para mas detalles"; } ?></font>
         </p>
      </div>


      <div id="box_caracteristicas_tecnicas">

          <table class="tabla_plazas">
                        <?php if( $page == "autocaravanas" || $page == "alquiler-autocaravanas" || $page == "ocasion"){ ?>
                        <tr><td>Plazas viajar:<td>
                        <?php
                          for ($i = $row_modelo['plazas_viajar']; $i > 0; $i--){
                            ?>
                                <img src="/img/ico_plazas.bmp" alt="Icono plazas autocaravana">
                            <?php
                          }
                        ?>
                        <?php } ?>
                        <tr><td>Plazas dormir:<td>
                        <?php
                          for ($i = $row_modelo['plazas_dormir']; $i > 0; $i--){
                            ?>
                                <img src="/img/ico_plazas.bmp" alt="Icono plazas autocaravana">
                            <?php
                          }
                        ?>
          </table>

        <table class="tabla_caracteristicas">
            <tr><td>Altura:<td><?php echo $row_modelo['dim_altura']; ?>
            <tr><td>Anchura:<td><?php echo $row_modelo['dim_anchura']; ?>
            <tr><td>Longitud:<td><?php echo $row_modelo['dim_longitud']; ?>
            <tr><td>Tara:<td><?php echo $row_modelo['peso_tara']; ?>
            <tr><td>MMA:<td><?php echo $row_modelo['peso_mma']; ?>
            <tr><td>Cama delantera:<td><?php echo $row_modelo['dim_cama_delantera']; ?>
            <tr><td>Cama central:<td><?php echo $row_modelo['dim_cama_central']; ?>
            <tr><td>Cama trasera:<td><?php echo $row_modelo['dim_cama_trasera']; ?>
            <tr><td>Cama literas:<td><?php echo $row_modelo['dim_cama_literas']; ?>
            <tr><td>Chasis:<td><?php echo $row_modelo['chasis']; ?>
            <?php if( $page == "autocaravanas" || $page == "alquiler-autocaravanas" ){ ?>
            <tr><td>Motor:<td><?php echo $row_modelo['motor']; ?>
            <?php } ?>
        </table>
          <!--
          <table class="tabla_caracteristicas">
             <tr><td>Motor:<td><?php echo $row['motor']; ?>
             <tr><td>Chasis<td><?php echo $row['chasis']; ?>
             <tr><td>Tara:<td><?php echo $row['tara']; ?> kg
             <tr><td>MMA:<td><?php echo $row['mma']; ?> kg
             <tr><td>Longitud:<td><?php echo $row['longitud']; ?> m
             <tr><td>Anchura:<td><?php echo $row['anchura']; ?> m
             <tr><td>Altura:<td><?php echo $row['altura']; ?> m
             <tr><td>Dim. cama delantera<td><?php echo $row['dim_cama_del']; ?>
             <tr><td>Dim. cama central<td><?php echo $row['dim_cama_cen']; ?>
             <tr><td>Dim. cama trasera<td><?php echo $row['dim_cama_tras']; ?>
             <tr><td>Dim. literas<td><?php echo $row['dim_cama_lit']; ?>
          </table>
          -->

     </div>

    </div>



   <?php
   $conn->close();
}

function listadoAutocaravanasGama($page,$table,$marca){

    if($page == "autocaravanas"){
        $query_gamas = "SELECT g.id AS id_gama,g.nombre AS nombre_gama,gi.nombre AS gama_imagen
            FROM autocaravanas_gama g 
            JOIN autocaravanas_marca m
            JOIN autocaravanas_gama_imagen gi
            JOIN imagen_tipo it
            WHERE g.id_marca = m.id 
            AND gi.id_element = g.id
            AND gi.id_tipo = it.id
            AND it.nombre = 'boton'
            AND m.nombre = '" . $marca . "'";
        
        $table_gamas = "autocaravanas_gama";
    }
    elseif($page == "caravanas"){
        $query_gamas = "SELECT g.id AS id_gama,g.nombre AS nombre_gama,gi.nombre AS gama_imagen
            FROM caravanas_gama g 
            JOIN caravanas_marca m
            JOIN caravanas_gama_imagen gi
            JOIN imagen_tipo it
            WHERE g.id_marca = m.id 
            AND gi.id_element = g.id
            AND gi.id_tipo = it.id
            AND it.nombre = 'boton'
            AND m.nombre = '" . $marca . "'";

        $table_gamas = "caravanas_gama";
    }

    require(realpath('controlv2/config/db.php'));

    $db_schema = $config['db']['v2']['dbname'];
    $db_user = $config['db']['v2']['users']['rw']['username'];
    $db_pass = $config['db']['v2']['users']['rw']['password'];
    $db_host = $config['db']['v2']['host'];

    $connection = new mysqli($db_host,$db_user,$db_pass,$db_schema);

    if ($res_autocaravanas = $connection->query($query_gamas)) {
        if ($res_autocaravanas->num_rows > 0){
            while($row_gama = mysqli_fetch_array($res_autocaravanas, MYSQLI_ASSOC)){
                $beauty_gama_name = strtolower(str_replace(' ','-',$row_gama['nombre_gama']));
                $dirImagesGama = $config['images']['rootDir'] . '/' . $config['images'][$table_gamas]['rootDir'] . '/' . $row_gama['id_gama'];
                ?>
                    <div class="btn_container_25">
                        <div class="btn_gamas">
                            <a href="/<?php echo $page; ?>/<?php echo $marca; ?>/<?php echo $beauty_gama_name; ?>">
                                <div class="btn_gamas_img">
                                    <img src="/<?php echo $dirImagesGama; ?>/<?php echo $row_gama['gama_imagen']; ?>"
                                         alt="<?php echo ucfirst($page); ?> <?php echo $marca; ?> gama <?php echo $row_gama['nombre_gama']; ?>">
                                    </img>
                                </div>
                                <div class="btn_gamas_texto">
                                    <font><?php echo $row_gama['nombre_gama']; ?></font>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php
            }
        }
    }

    $connection->close();

}

function listadoModelosGama($page,$table,$marca,$gama){

    require(realpath('controlv2/config/db.php'));

    $db_schema = $config['db']['v2']['dbname'];
    $db_user = $config['db']['v2']['users']['rw']['username'];
    $db_pass = $config['db']['v2']['users']['rw']['password'];
    $db_host = $config['db']['v2']['host'];

    $imgTable = $table . '_imagen';
    $imagesDir =  $config['images']['rootDir'] . '/' . $config['images'][$table]['rootDir'];

    if($page == "autocaravanas"){
        $query_modelos = "SELECT t.nombre as nombre,t.temporada as temporada,t.id as id,m.nombre as marca,g.nombre as gama,tt.nombre as tipo
            FROM " . $table . " t 
            JOIN autocaravanas_marca m
            JOIN autocaravanas_gama g
            JOIN autocaravanas_tipo tt
            WHERE m.id = t.id_marca
            AND g.id = t.id_gama
            AND t.id_tipo = tt.id
            AND m.nombre = '". $marca . "'
            AND LOWER(REPLACE(g.nombre,' ','-')) = '" . $gama . "'
            ORDER BY t.nombre";

        $element_name = "autocaravana";
    }
    elseif($page == "caravanas"){
        $query_modelos = "SELECT t.nombre as nombre,t.temporada as temporada,t.id as id,m.nombre as marca,g.nombre as gama
            FROM " . $table . " t 
            JOIN caravanas_marca m
            JOIN caravanas_gama g
            WHERE m.id = t.id_marca
            AND g.id = t.id_gama
            AND m.nombre = '". $marca . "'
            AND LOWER(REPLACE(g.nombre,' ','-')) = '" . $gama . "'
            ORDER BY t.nombre";
        
        $element_name = "caravana";
    }

    $connection = new mysqli($db_host,$db_user,$db_pass,$db_schema);

    //Mostrar cabecera
    $query_cabecera_gama = "SELECT g.id AS id_gama,gi.nombre AS nombre,it.nombre AS tipo 
                            FROM " . $element_name . "s_gama g
                                JOIN " . $element_name . "s_marca m
                                JOIN " . $element_name . "s_gama_imagen gi 
                                JOIN imagen_tipo it 
                            WHERE g.id = gi.id_element 
                                AND gi.id_tipo = it.id
                                AND m.id = g.id_marca
                                AND m.nombre = '". $marca . "'
                                AND it.nombre IN ('auxiliar-1','auxiliar-2') 
                                AND LOWER(REPLACE(g.nombre,' ','-')) = '" . $gama . "'
                            ORDER BY it.nombre ASC";
    
    //SELECT gi.nombre AS nombre,it.nombre AS tipo FROM autocaravanas_gama g JOIN autocaravanas_marca m JOIN autocaravanas_gama_imagen gi JOIN imagen_tipo it WHERE g.id = gi.id_element AND gi.id_tipo = it.id AND m.id = g.id_marca AND m.nombre = 'burstner' AND it.nombre IN ('auxiliar-1','auxiliar-2') AND LOWER(REPLACE(g.nombre,' ','-')) = 'lyseo-time-i'
    if ($res_cabecera_gama = $connection->query($query_cabecera_gama)) {
        if ($res_cabecera_gama->num_rows > 0){
            ?>
            <div class="container_100_percent">
            <?php
            while($row_cabecera_gama = mysqli_fetch_array($res_cabecera_gama, MYSQLI_ASSOC)){
                ?><?php
                $imagesGamaDir =  $config['images']['rootDir'] . '/' . $config['images'][$element_name . "s_gama"]['rootDir'] . '/' . $row_cabecera_gama['id_gama'];
                $imagePath = '/' . $imagesGamaDir . '/' . $row_cabecera_gama['nombre'];
                ?>
                    <div class="container_50_percent">
                        <img src="<?php echo $imagePath; ?>"></img>
                    </div>
                <?php
            }
            ?>
            </div>
            <?php
        }
    }

    if ($res_modelos = $connection->query($query_modelos)) {
        if ($res_modelos->num_rows > 0){
            while($row_modelo = mysqli_fetch_array($res_modelos, MYSQLI_ASSOC)){
                $query_image = "SELECT t.nombre AS tipo,i.nombre AS nombre 
                                FROM " . $imgTable . " i 
                                JOIN imagen_tipo t 
                                WHERE i.id_tipo = t.id
                                    AND i.id_element = '" . $row_modelo['id'] . "' 
                                    AND t.nombre = 'auxiliar-2';";
                
                $res_image = $connection->query($query_image);
                if ($res_modelos->num_rows > 0){
                    $row_image = mysqli_fetch_array($res_image, MYSQLI_ASSOC);
                    $imgPlanta = '/' . $imagesDir . '/' . $row_modelo['id'] . '/' . $row_image['nombre'];
                }
                
                $model_str = $row_modelo['id'] . "-" . $row_modelo['nombre'] . "-" . $row_modelo['temporada'];

                ?>
                    <a href="/<?php echo $page; ?>/<?php echo $row_modelo['marca']; ?>/<?php echo $gama; ?>/<?php echo $model_str; ?>">
                        <div class="btn_pers_weinsberg">
                            <div>
                                <img src="<?php echo $imgPlanta; ?>" 
                                    alt="Planta <?php echo $element_name; ?> <?php echo $row_modelo['marca']; ?> modelo <?php echo $row_modelo['nombre']; ?>">
                                </img>
                            </div>
                            <div style="text-align:center;">
                                <font style="font-family:Arial;font-size:20px;font-weight:bold;"><?php echo $row_modelo['nombre']; ?></font>
                            </div>
                        </div>
                    </a>
                <?php
            }
        }
    }
}

function listadoAutocaravanasMarca($page,$table,$marca){

    require(realpath('controlv2/config/db.php'));

    $db_schema = $config['db']['v2']['dbname'];
    $db_user = $config['db']['v2']['users']['rw']['username'];
    $db_pass = $config['db']['v2']['users']['rw']['password'];
    $db_host = $config['db']['v2']['host'];

    $conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
    if ($mysqli->connect_errno) {
        echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    $imgTable = $table . '_imagen';
    $imagesDir =  $config['images']['rootDir'] . '/' . $config['images'][$table]['rootDir'];

    switch($marca){
        case "all":
            $query_autocaravanas = str_replace('?',$marca,$config['db']['v2']['tables'][$table]['detail_query']);
            $marcaURL = 'stock';
            break;
        default:        
            $query_autocaravanas = str_replace('?',$marca,$config['db']['v2']['tables'][$table]['detail_query_marca']);
            $marcaURL = $marca;
    }

    if ($res_autocaravanas = $conn->query($query_autocaravanas)) {
        if ($res_autocaravanas->num_rows > 0){
            while($row_autocaravana = mysqli_fetch_array($res_autocaravanas, MYSQLI_ASSOC)){

                $img_upper_right = "";
                $img_upper_left = "";
                $img_bottom_left = "";
                $img_bottom_right = "";
                $logoMarca = "";

                $query_images = "SELECT t.nombre AS tipo,i.nombre AS nombre 
                                 FROM " . $imgTable . " i 
                                 JOIN imagen_tipo t 
                                 WHERE i.id_tipo = t.id
                                 AND i.id_element = '" . $row_autocaravana['id'] . "' 
                                 AND t.nombre IN ('principal','planta-dia','planta-noche','auxiliar-1','auxiliar-2','auxiliar-3','auxiliar-4');";
                
                                 /*
                $query_marca_images = "SELECT t.nombre AS tipo,i.nombre AS nombre 
                                       FROM autocaravanas_marca_imagen i 
                                       JOIN imagen_tipo t 
                                       WHERE i.id_tipo = t.id
                                       AND i.id_element = '" . $row_autocaravana['id_marca'] . "' 
                                       AND t.nombre IN ('logo');";

                if ($res_marca_images = $conn->query($query_marca_images)) {
                    while($row_marca_image = mysqli_fetch_array($res_marca_images, MYSQLI_ASSOC)){
                        if($row_marca_image['tipo' ] == "logo"){
                            $logoMarca = '/' . $config['images']['rootDir'] . '/' . $config['images']['autocaravanas_marca']['rootDir'] . '/' . $row_autocaravana['id_marca'] . '/' . $row_marca_image['nombre'];
                        }
                    }
                }*/

                //Carga de imagenes auxiliares
                if ($res_images = $conn->query($query_images)) {
                    while($row_image = mysqli_fetch_array($res_images, MYSQLI_ASSOC)){
                        $tipoImg = $row_image['tipo' ];
                        switch($tipoImg){
                            case 'auxiliar-1':
                                $img_upper_left = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/' . $row_image['nombre'];
                                break;
                            case 'auxiliar-2':
                                $img_upper_right = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/' . $row_image['nombre'];
                                break;
                            case 'auxiliar-3':
                                $img_bottom_left = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/' . $row_image['nombre'];
                                break;
                            case 'auxiliar-4':
                                $img_bottom_right = '/' . $imagesDir . '/' . $row_autocaravana['id'] . '/' . $row_image['nombre'];
                                break;
                        }
                    }
                }

                if($page == "ocasion"){
                    $title_str = $row_autocaravana['nombre'];
                    $beauty_element_name = $row_autocaravana['id'] . '-' . strtolower(str_replace(' ','-',$row_autocaravana['nombre'])) . '-' . $row_autocaravana['temporada']; 
                    $element_path = $_SERVER['REQUEST_URI'] . '/' . $beauty_element_name;
                }
                else{
                    $title_str = ucfirst($row_autocaravana['marca']) . " " . ucfirst($row_autocaravana['gama']) . " " . $row_autocaravana['nombre'];
                    $beauty_element_name = $row_autocaravana['id'] . '-' . strtolower(str_replace(' ','-',$row_autocaravana['gama'])) . '-' . $row_autocaravana['nombre'] . '-' . $row_autocaravana['temporada']; 
                    $element_path = $_SERVER['REQUEST_URI'] . '/' . $row_autocaravana['marca'] . '/' . $beauty_element_name;
                }

                if($row_autocaravana['visible'] > 0){ //ini visible switch
                ?>
                    <a href="<?php echo $element_path; ?>">
                    <div id="c_elem_detail">
                        <div id="elem_detail">
                            <div id="elem_detail_title">
                                <?php echo $title_str; ?>
                            </div>
                            <div id="elem_detail_img_upper_left">
                                <img src="<?php echo $img_upper_left; ?>"></img>
                            </div>
                            <div id="elem_detail_img_upper_right">
                                <img src="<?php echo $img_upper_right; ?>"></img>
                            </div>
                            <div id="elem_detail_img_bottom_left">
                                <img src="<?php echo $img_bottom_left; ?>"></img>
                            </div>
                            <div id="elem_detail_img_bottom_right">
                                <img src="<?php echo $img_bottom_right; ?>"></img>
                            </div>
                        </div>
                    </div>
                    </a>
                <?php
                } //end visible switch
            }
        }
    }

    $conn->close();

}

function listadoMarcas($page,$table){
	
    require(realpath('controlv2/config/db.php'));

    $db_schema = $config['db']['v2']['dbname'];
    $db_user = $config['db']['v2']['users']['rw']['username'];
    $db_pass = $config['db']['v2']['users']['rw']['password'];
    $db_host = $config['db']['v2']['host'];

    $conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
    if ($mysqli->connect_errno) {
        echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }


    $imgTable = $table . '_imagen';
    $imagesDir =  $config['images']['rootDir'] . '/' . $config['images'][$table]['rootDir'];
    $query_marcas = $config['db']['v2']['tables'][$table]['list_query'];

    if ($res_marcas = $conn->query($query_marcas)) {
        if ($res_marcas->num_rows > 0){
            while($row_marca = mysqli_fetch_array($res_marcas, MYSQLI_ASSOC)){

                $img_boton = "";
                $query_boton = "SELECT mi.nombre,m.webpage,m.redirect,m.visible
                                FROM " . $table . " m join " . $imgTable . " mi 
                                WHERE m.id = mi.id_element 
                                AND mi.id_tipo = (select id from imagen_tipo where nombre = 'boton') 
                                AND m.id = '" . $row_marca['id'] . "';";
                if ($res_boton = $conn->query($query_boton)) {
                    if ($res_boton->num_rows > 0){
                        $row_boton = mysqli_fetch_array($res_boton, MYSQLI_ASSOC);
                        $img_boton = $row_boton['nombre'];
                    }
                }

                $dirImagesMarca = $imagesDir . '/' . $row_marca['id'];

                if($row_boton['redirect'] > 0){ 
                    $link_dest = $row_boton['webpage']; 
                    $link_mode = "_blank";
                }
                else{ 
                    $link_dest = '/' . $page . '/' . $row_marca['nombre'];
                    $link_mode = "_self";
                }
                if($row_marca['visible'] > 0){
                ?>
                <div class="btn_marca_autocaravana">
                <a href="<?php echo $link_dest; ?>" target="<?php echo $link_mode; ?>">
                    <img src="/<?php echo $dirImagesMarca; ?>/<?php echo $img_boton; ?>" alt="<?php echo $page; ?> marca <?php echo $row_marca['nombre']; ?>"></img>
                </a>
                </div>
                <?php
                }
            }
        }
    }
    else{
        echo 'No se han encontrado marcas';
    }

    $conn->close();

}

