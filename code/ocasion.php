<?php

require_once(realpath('code/include/functions.php'));

?>

<!--INICIO contenedor_seccion-->
<div class="contenedor_seccion">

<?php

//Localizacion
$page = $_GET['id'];

if( $page == "ocasion" && !isset($_GET['modelo']) ){

    ?>

    <div class="titulo_seccion"><h1>Ocasion</h1></div>
    <?php

    listadoAutocaravanasMarca($page,'ocasion','all');

}
elseif( $page == "ocasion" && isset($_GET['modelo']) ){

    $modeltemp = $_GET['modelo'];
    $modeltemparr = explode('-',$modeltemp);
    $modelo = "";

    for ($i = 0; $i < count($modeltemparr); $i++) {
        switch(true){
            case ( $i == 0 ):
                $id = $modeltemparr[$i];
                break;
            case ( $i < ( count($modeltemparr) - 1 ) && $i > 0 ):
                $modelo = $modelo . ' ' . ucfirst($modeltemparr[$i]);
                break;
            case ( $i == ( count($modeltemparr) - 1 ) ):
                $temporada = $modeltemparr[$i];
                break;
        }
    }

    ?>
    <!--TITULO SECCION-->
    <div class="titulo_seccion">
        <div style="float:left;margin-left:5px;">
            <font>
                <a href="/<?php echo $page; ?>" rel="nofollow"><h1>Ocasion</h1></a> > <h2><?php echo $modelo; ?></h2>
            </font>
        </div>
        <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
            <a href="/<?php echo $page; ?>" rel="nofollow">
                <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
                <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" alt="Flecha atras" style="max-height:25px;"></img></div>
            </a>
        </div>
    </div>
    <?php

    detalleAutocaravana($page,'ocasion',$id);

}else{
    echo "Error";
}

?>
</div>
<!--FIN contenedor_seccion-->