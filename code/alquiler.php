<?php

include 'code/dbconnect.php';include 'code/properties.php';
require_once(realpath('code/include/functions.php'));

//Localizacion
$page = $_GET['id'];

if ( ! isset( $_GET['acc'] ) ){

?>

  <div class="titulo_seccion"><font><h1>Alquiler</h1></font></div>

   <div id="contenedor_botones_alquiler">
   <div id="contenedor_50">
     <a href="alquiler-autocaravanas/ver-vehiculos">
       <img src="/img/db/alquiler/alquiler_btn_vehiculos.png" alt="Alquiler autocaravanas - ver vehiculos"/>
     </a>
   </div>
   <div id="contenedor_50">
     <a href="alquiler-autocaravanas/condiciones">
       <img src="/img/db/alquiler/alquiler_btn_condiciones.png" alt="Alquiler autocaravanas - condiciones"/>
     </a>
   </div>
   </div>

   <div id="img_ancha" class="zoom_icon">
      <a onclick='loadPopup("/img/db/alquiler/calendario.jpg","imagen")'>
         <img title="Calendario alquiler | Nusa Caravaning" alt="Alquiler autocaravanas - Calendario" src="/img/db/alquiler/calendario.jpg"/>
      </a>
   </div>

   <!--<div id="img_ancha">
        <img src="/img/db/alquiler/packs.jpg" alt="Alquiler autocaravanas - Packs incluidos"></img>
   </div>-->

   <div id="img_ancha">
     <a href="alquiler-autocaravanas/razones">
        <img src="/img/alquiler_boton_razones.gif" alt="Alquiler autocaravanas - razones"></img>
     </a>
   </div>

   <script language="javascript" type="text/javascript">
     function loadPopup( contenido , mode ){
          if(mode == 'video'){
             url = "<iframe id='elem_cent_item_iframe' src='https://www.youtube.com/embed/"+contenido+"?rel=0&wmode=transparent&autoplay=1'></iframe>" ;
          }
          if(mode == 'imagen'){
             url = "<img src='"+contenido+"'/>";
          }
          document.getElementById("elem_cent_item").innerHTML = url;
          document.getElementById("elem_cent_contenedor").style.display = "table";
          document.getElementById("elem_cent_fondo").style.display = "block";
     }
     function closePopup(){
          document.getElementById("elem_cent_contenedor").style.display = "none";
          document.getElementById("elem_cent_fondo").style.display = "none";
          document.getElementById("elem_cent_item_iframe").src = "";   
     }
   </script>
   
<?php

}
elseif ( $_GET['acc'] == "razones" ){

   ?> 

   <!--TITULO SECCION-->
  <div class="titulo_seccion">
     <div style="float:left;margin-left:5px;">
     <font>
       <a href="/<?php if($_SERVER['REDIRECT_env_provincia']){echo $_SERVER['REDIRECT_env_provincia'] . "/";} ?>alquiler-autocaravanas" rel="nofollow"><h1>Alquiler</h1></a> > <h2>Razones para alquilar</h2>
     </font>
     </div>
     <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
       <a href="/<?php if($_SERVER['REDIRECT_env_provincia']){echo $_SERVER['REDIRECT_env_provincia'] . "/";} ?>alquiler-autocaravanas" rel="nofollow">
         <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
         <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" style="max-height:25px;" alt="Flecha atras"></img></div>
       </a>
     </div>
  </div>

   <div id="cuadro_razones">
    <div class="cuadro_razones_titulo">Razones para alquilar en Nusa Caravaning</div> 
    <ul>
      <li class="indent">La &uacute;nica condici&oacute;n indispensable es ser mayor de 23 a&ntilde;os y al menos dos a&ntilde;os de posesi&oacute;n del carnet B</li>
      <li>Nuestros veh&iacute;culos son de gama alta y nueva colecci&oacute;n, nunca outlet</li>
      <li class="indent">Los veh&iacute;culos nunca superan los dos a&ntilde;os de antig&uuml;edad, llegando a renovarlos, en ocasiones, hasta de forma anual</li>
      <li>Todos los veh&iacute;culos son equipados con frigor&iacute;ficos grandes, trivalentes y autom&aacute;ticos, por lo que no deber&aacute; preocuparse de cambiar su modo de funcionamiento, su nevera siempre permanecer&aacute; fr&iacute;a</li>
      <li class="indent">Los veh&iacute;culos son fabricados sobre chasis "camping car"</li>
      <li>Los reguladores de gas son sustituidos por sistemas "duo control cs"; elemento de seguridad que nos permitir&aacute; llevar abierto el gas en viaje</li>
      <li class="indent">Los veh&iacute;culos ir&aacute;n complementados con una gu&iacute;a de &aacute;reas de autocaravanas, para facilitarles la b&uacute;squeda</li>
      <li>Se les facilitar&aacute; un n&uacute;mero de tel&eacute;fono, operativo las 24 horas del d&iacute;a, para cualquier imprevisto o duda que les pueda surgir durante el viaje</li>
    </ul>
   </div> 

   <?php

}
elseif ( $_GET['acc'] == "condiciones" ){
   
   ?>

   <!--TITULO SECCION-->
  <div class="titulo_seccion">
     <div style="float:left;margin-left:5px;">
     <font>
       <a href="/<?php if($_SERVER['REDIRECT_env_provincia']){echo $_SERVER['REDIRECT_env_provincia'] . "/";} ?>alquiler-autocaravanas" rel="nofollow"><h1>Alquiler</h1></a> > <h2>Condiciones</h2>
     </font>
     </div>
     <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
       <a href="/<?php if($_SERVER['REDIRECT_env_provincia']){echo $_SERVER['REDIRECT_env_provincia'] . "/";} ?>alquiler-autocaravanas" rel="nofollow">
         <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
         <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" style="max-height:25px;" alt="Flecha atras"></img></div>
       </a>
     </div>
  </div>

   <div id="condiciones_texto">
     <p>
     <ul>
       <li>El conductor del veh&iacute;culo deber&aacute; ser mayor de 23 a&ntilde;os y estar en posesi&oacute;n de un permiso de conducir clase B y con m&aacute;s de 2 a&ntilde;os de antig&uuml;edad. 
       <li>La entrega y recogida del veh&iacute;culo se efectuar&aacute; en las instalaciones de NUSA CARAVANING S.L. dentro del horario laboral del arrendador.
       <li>El importe total del alquiler contratado deber&aacute; estar abonado antes de la salida. As&iacute; mismo deber&aacute; estar depositada una fianza de 540,00 euros mediante tarjeta de cr&eacute;dito para garantizar el buen uso y correcta devoluci&oacute;n del veh&iacute;culo.
       <li>Queda expresamente prohibido llevar animales y m&aacute;s pasajeros que los especificados en la documentaci&oacute;n del veh&iacute;culo, as&iacute; como, viajar a cualquier pa&iacute;s que se encuentre en guerra o conflictos b&eacute;licos.
       <li>El usuario responde de cualquier multa o sanci&oacute;n que se produzca dentro del periodo de alquiler.
       <li>Los gastos de carburante son por cuenta del arrendatario. El veh&iacute;culo se entrega con el dep&oacute;sito de combustible lleno y as&iacute; debe devolverse.
       <li>El veh&iacute;culo se devolver&aacute; limpio interiormente y con los dep&oacute;sitos de aguas residuales y WC vac&iacute;os. En caso contrario se cobrar&aacute; un suplemento de 100,00 euros en concepto de limpieza.
       <li>En caso de que el arrendatario, por propia decisi&oacute;n unilateral inicie con retraso o termine anticipadamente el alquiler, no tendr&aacute; derecho a reembolso alguno. As&iacute; como, por cualquier otro periodo de alquiler no utilizado.
       <li>Las presentes condiciones son s&oacute;lo un extracto, siendo a todos los efectos &uacute;nicamente v&aacute;lidas las que se firmen en el correspondiente contrato de alquiler.
     </ul>
     </p>
   </div>
   <div id="condiciones_imagen">
    <br>
    <img src="/img/alquiler_condiciones_generales.jpg" alt="Alquiler autocaravanas - Condiciones generales"></img>
   </div>
   <?php

}
elseif ( $_GET['acc'] == "ver-vehiculos" && ! isset( $_GET['marca'] ) && ! isset( $_GET['tipo'] ) && ! isset( $_GET['modelo'] ) ){

   ?>

   <!--TITULO SECCION-->
  <div class="titulo_seccion">
     <div style="float:left;margin-left:5px;">
     <font>
       <a href="/<?php if($_SERVER['REDIRECT_env_provincia']){echo $_SERVER['REDIRECT_env_provincia'] . "/";} ?>alquiler-autocaravanas" rel="nofollow"><h1>Alquiler</h1></a> > <h2>Ver veh&iacute;culos</h2>
     </font>
     </div>
     <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
       <a href="/<?php if($_SERVER['REDIRECT_env_provincia']){echo $_SERVER['REDIRECT_env_provincia'] . "/";} ?>alquiler-autocaravanas" rel="nofollow">
         <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
         <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" style="max-height:25px;" alt="Flecha atras"></img></div>
       </a>
     </div>
  </div>

  <?php 

    listadoAutocaravanasMarca($page,'autocaravanas_alquiler','all');   

}
elseif ( $_GET['acc'] == "ver-vehiculos" && isset( $_GET['marca'] ) && isset( $_GET['modelo'] ) ){

    $marca = $_GET['marca'];
    $modeltemp = $_GET['modelo'];
    $modeltemparr = explode('-',$modeltemp);
    $modelo = "";

    for ($i = 0; $i < count($modeltemparr); $i++) {
        switch(true){
            case ( $i == 0 ):
                $id = $modeltemparr[$i];
                break;
            case ( $i < ( count($modeltemparr) - 1 ) && $i > 0 ):
                $modelo = $modelo . ' ' . ucfirst($modeltemparr[$i]);
                break;
            case ( $i == ( count($modeltemparr) - 1 ) ):
                $temporada = $modeltemparr[$i];
                break;
        }
    }

   ?>

  <!--TITULO SECCION-->
    <div class="titulo_seccion">
        <div style="float:left;margin-left:5px;">
            <font>
                <a href="/alquiler-autocaravanas" rel="nofollow"><h1>Alquiler</h1></a> > <a href="/alquiler-autocaravanas/ver-vehiculos"><h2>Ver veh&iacute;culos</h2></a> > <h3><?php echo strtoupper($marca); ?>  <?php echo $modelo; ?> (<?php echo $temporada; ?>)</h3>
            </font>
        </div>
        <div style="overflow:hidden;float:right;margin:0 5px 0 5px;align:right;">
            <a href="/alquiler-autocaravanas/ver-vehiculos" rel="nofollow">
                <div style="float:right;margin:0 4px 0 4px;"><font style="line-height:25px;">Atr&aacute;s</font></div>
                <div style="float:right;margin:0 4px 0 4px;"> <img src="/img/back.png" alt="Flecha atras" style="max-height:25px;"></img></div>
            </a>
        </div>
    </div>

  <?php 

  detalleAutocaravana($page,'autocaravanas_alquiler',$id,0);

}

