<footer id="pie">

<div id="pie_social">
  <font>S&iacute;guenos ... </font>
  <img id="scale" src="/img/ico_facebook.png" alt="S&#237;guenos | Nusa Caravaning"> 
  <img id="scale" src="/img/ico_google.png" alt="S&#237;guenos | Nusa Caravaning">
  <img id="scale" src="/img/ico_twitter.png" alt="S&#237;guenos | Nusa Caravaning">
  <img id="scale" src="/img/ico_instagram.png" alt="S&#237;guenos | Nusa Caravaning">
  <img id="scale" src="/img/ico_youtube.png" alt="S&#237;guenos | Nusa Caravaning"> 
  <div class="fb-like"
       data-href="https://www.facebook.com/NusaCaravaning"
       data-width="150" data-layout="button_count"
       data-action="like"
       data-show-faces="true"
       data-share="false"
       id="btn_facebook">
  </div>
</div>

<div id="pie_servicios">
<div id="pie_seccion">
   <a href="/caravanas">CARAVANAS</a>
   <br>
   <ul class="submenu">
     <li><a href="/caravanas/weinsberg">Caravanas Weinsberg</a>
     <li><a href="/caravanas/fendt">Caravanas Fendt</a>
     <li><a href="/caravanas/burstner">Caravanas Burstner</a>
   </ul>
</div>

<div id="pie_seccion">
   <a href="/autocaravanas">AUTOCARAVANAS</a>
   <br>
   <ul class="submenu">
    <li><a href="/autocaravanas/burstner">Autocaravanas Burstner</a>
    <li><a href="/autocaravanas/giottiline">Autocaravanas Giottiline</a>
    <li><a href="/autocaravanas/pla/mister">Autocaravanas Mister</a>
    <li><a href="/autocaravanas/pla/happy">Autocaravanas Happy</a>
    <li><a href="/autocaravanas/etrusco">Autocaravanas Etrusco</a>
    <li><a href="/autocaravanas/carado">Autocaravanas Carado</a>
    <li><a href="/autocaravanas/pilote">Autocaravanas Pilote</a>
    <li><a href="/autocaravanas/weinsberg">Autocaravanas Weinsberg</a>
   </ul>
</div>

<div id="pie_seccion">
   <a href="/ocasion">OCASION</a> 
   <br>
   <ul class="submenu">
     <li>Autocaravanas de ocasi&oacute;n 
     <li>Caravanas de ocasi&oacute;n
   </ul>

</div>

<div id="pie_seccion">
   <a href="/alquiler-autocaravanas">ALQUILER AUTOCARAVANAS</a>
   <br>
   <ul class="submenu">
     <a href="/Valladolid/alquiler-autocaravanas"><li>Alquiler autocaravanas Valladolid</a>
     <a href="/Zamora/alquiler-autocaravanas"><li>Alquiler autocaravanas Zamora</a>
     <a href="/Avila/alquiler-autocaravanas"><li>Alquiler autocaravanas Avila</a>
     <a href="/Palencia/alquiler-autocaravanas"><li>Alquiler autocaravanas Palencia</a> 
     <a href="/Segovia/alquiler-autocaravanas"><li>Alquiler autocaravanas Segovia</a>
     <a href="/Burgos/alquiler-autocaravanas"><li>Alquiler autocaravanas Burgos</a>
     <a href="/Salamanca/alquiler-autocaravanas"><li>Alquiler autocaravanas Salamanca</a>
     <a href="/Leon/alquiler-autocaravanas"><li>Alquiler autocaravanas Leon</a>
   </ul>
</div>

<div id="pie_seccion">
   <a href="/servicio-tecnico">SERVICIO TECNICO</a> 
</div>     

<div id="pie_seccion">
   <a href="/instalacion-enganches">INSTALACION ENGANCHES</a>
   <br>
   <ul class="submenu">
     <li>instalacion enganche fijo
     <li>instalacion enganche desmontable
     <li>instalacion enganche escamoteable
   </ul> 
</div> 

<div id="pie_seccion">
   <a href="/parking">PARKING</a>
</div> 
</div>

<div id="pie_informacion">
<div id="pie_informacion_datos_empresa">
 <div class="pie_informacion_logo">
   <img src="/img/logo_nusa.png"/>
 </div>
 <font class="pie_informacion_empresa">
   Nusa Caravaning S.L.
 </font>
 <font class="pie_informacion_direccion">
   <br>Urbanizaci&oacute;n Las Tinajas 47239 Villanueva de Duero <br>( VALLADOLID )<br>
 </font>
</div>
<div id="pie_informacion_datos_misc">
 <font class="pie_informacion_contacto">
   <b>Tel&eacute;fono:</b> 983 555 881<br><b>Fax:</b> 983 555 843<br><b>M&oacute;vil:</b> 649 987 170
 </font>
 <font class="pie_informacion_mail">
   <br><b>Correo electr&oacute;nico: </b><a href="mailto:info@nusacaravaning.com" class="pie_enlace">info@nusacaravaning.com</a>
 </font>
</div>
</div>

</footer> <!-- fin pie -->
