<?php

include 'code/dbconnect.php';include 'code/properties.php';
require(realpath('code/include/functions.php'));

//Localizacion
$page = $_GET['id'];

$empresa_str = "Nusa Caravaning";
$url = "https://www.nusacaravaning.com" . $_SERVER['REQUEST_URI'];
$ini_str = "";
$title_str = "Alquiler, venta y servicio técnico de caravanas y autocaravanas en Valladolid";
$desc_str = "Alquiler, venta y servicio técnico de caravanas y autocaravanas en Valladolid";
$image = "";

//Inicializacion marcas
$autocaravanas_marcas = ["Burstner","Giottiline","PLA","Etrusco","Carado","Pilote","Weinsberg"];
$caravanas_marcas = ["Weinsberg","Knaus","Fendt","Burstner"];

//Obtener provincia
$uri_parts = explode('/',$_SERVER['REQUEST_URI']);
switch($uri_parts[1]){
    case 'Zamora':
        $prov_alquiler = 'Zamora';
        break;
    case 'Segovia':
        $prov_alquiler = 'Segovia';
        break;
    case 'Salamanca':
        $prov_alquiler = 'Salamanca';
        break; 
    case 'Avila':
        $prov_alquiler = 'Avila';
        break;
    case 'Soria':
        $prov_alquiler = 'Soria';
        break;
    case 'Palencia':
        $prov_alquiler = 'Palencia';
        break; 
    case 'Leon':
        $prov_alquiler = 'Leon';
        break; 
    case 'Burgos':
        $prov_alquiler = 'Burgos';
        break;   
    default:
        $prov_alquiler = 'Valladolid';
}

switch($page){
    case 'autocaravanas':
        if(isset($_GET['stock'])){
            $table = "autocaravanas_stock"; 
        }    
        else{
            $table = "autocaravanas_catalogo";
        }
        break;
    case 'caravanas':
        if(isset($_GET['stock'])){
            $table = "caravanas_stock"; 
        }    
        else{
            $table = "caravanas_catalogo";
        }
        break;
    case 'alquiler-autocaravanas':
        $table = "autocaravanas_alquiler";
        $ini_str = "ALQUILER " . $prov_alquiler . " | ";
        break;
    case 'ocasion':
        $table = "ocasion";
        break;
}

if(isset($_GET['modelo'])){
    $modeltemp = $_GET['modelo'];
    $modeltemparr = explode('-',$modeltemp);
    $id = $modeltemparr[0];
    if(isset($_GET['stock'])){
        $ini_str = "STOCK  | "; 
    }elseif($_GET['id'] == "ocasion"){
        $ini_str = "OCASION | ";
    }
    $title_str = $ini_str . getHeadInfo('title',$page,$table,$id) . " | " . $empresa_str;
    $desc_str = $ini_str . getHeadInfo('title',$page,$table,$id) . " con las mejores condiciones de financiacion. Ven a comprobarlo en nuestras instalaciones | " . $empresa_str;
    $image = "https://www.nusacaravaning.com" . getHeadInfo('image',$page,$table,$id);
}
elseif(isset($_GET['gama'])){
    $marca = $_GET['marca'];
    $gama = $_GET['gama'];
    $gamaStr = ucwords(implode(' ',explode('-',$gama)));

    $title_str = ucfirst($page) . " " . ucfirst($marca) . " " . $gamaStr . " | " . $empresa_str;
    $desc_str = "Venta de " . ucfirst($page) . " marca " . ucfirst($marca) . " gama " . $gamaStr . " con las mejores condiciones de financiacion. Ven a comprobarlo en nuestras instalaciones | " . $empresa_str;
}
elseif(isset($_GET['marca'])){
    $marca = $_GET['marca'];
    $title_str = ucfirst($page) . " " . ucfirst($marca) . " | " . $empresa_str;
    $desc_str = "Venta de " . ucfirst($page) . " marca " . ucfirst($marca) . " con las mejores condiciones de financiacion. Ven a comprobarlo en nuestras instalaciones | " . $empresa_str;
}
else{
    switch($page){
        case 'autocaravanas':
            $marcasStr = ucwords(implode(', ',$autocaravanas_marcas));
            if(isset($_GET['stock'])){
                $title_str = "Stock disponible - Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
                $desc_str = "Stock disponible - Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
            }
            else{
                $title_str = "Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
                $desc_str = "Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
            }
            break;
        case 'caravanas':
            $marcasStr = ucwords(implode(', ',$caravanas_marcas));
            if(isset($_GET['stock'])){
                $title_str = "Stock disponible - Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
                $desc_str = "Stock disponible - Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
            }
            else{
                $title_str = "Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
                $desc_str = "Venta de " . ucfirst($page) . " de gama alta " . $marcasStr . " | " . $empresa_str;
            }
            break;
        case 'alquiler-autocaravanas':
            switch($_GET['acc']){
                case 'ver-vehiculos':
                    $title_str = "Alquiler de autocaravanas " . $prov_alquiler . " - Vehículos disponibles | " . $empresa_str;
                    $desc_str = "Alquiler de autocaravanas " . $prov_alquiler . " - Vehículos disponibles | " . $empresa_str;
                    break;
                case 'condiciones':
                    $title_str = "Alquiler de autocaravanas " . $prov_alquiler . " - Condiciones | " . $empresa_str;
                    $desc_str = "Alquiler de autocaravanas " . $prov_alquiler . " - Condiciones | " . $empresa_str;
                    break;
                case 'razones':
                    $title_str = "Alquiler de autocaravanas " . $prov_alquiler . " - Razones para alquilar con nosotros | " . $empresa_str;
                    $desc_str = "Alquiler de autocaravanas " . $prov_alquiler . " - Razones para alquilar con nosotros | " . $empresa_str;
                    break;
                default:
                    $title_str = "Alquiler de autocaravanas de gama alta entre 2 y 6 plazas " . $prov_alquiler . " - Calendario | " . $empresa_str;
                    $desc_str = "Alquiler de autocaravanas de gama alta entre 2 y 6 plazas " . $prov_alquiler . " - Calendario | " . $empresa_str;
            }   
            break;
        case 'ocasion':
            $title_str = "Caravanas y autocaravanas de ocasion | " . $empresa_str;
            $desc_str = "Caravanas y autocaravanas ( perfiladas, capuchinas e integrales ) usadas en muy buen estado y a buen precio. Las puedes ver en nuestras instalaciones | " . $empresa_str;
            break;
        case 'instalacion-enganches':
            $title_str = "Instalacion de enganche fijo, desmontable y escamoteable barato | " . $empresa_str;
            $desc_str = "Le instalamos su enganche fijo, desmontable y escamoteable en su vehiculo en " . $prov_alquiler . ", con todas las garantias. Pidanos presupuesto para su vehiculo | " . $empresa_str;
            break;
        case 'servicio-tecnico':
            $title_str = "Servicio tecnico oficial de caravanas y autocaravanas en " . $prov_alquiler . " | " . $empresa_str;
            $desc_str = "Ofrecemos servicio t&eacute;cnico especializado de caravanas y autocaravanas en " . $prov_alquiler . ". Tratamos su vehiculo como si fuera nuestro y a precios muy economicos | " . $empresa_str;
            break;
        case 'parking':
            $title_str = "Parking para caravanas y autocaravanas en " . $prov_alquiler . " | " . $empresa_str;
            $desc_str = "Servicio de parking vigilado y asegurado para caravanas y autocaravanas en Valladolid, ademas de instalaciones para el lavado y cuidado de su vehiculo | " . $empresa_str;
            break;
        case 'accesorios':
            $title_str = "Accesorios para camping, caravanas, autocaravanas en " . $prov_alquiler . " | " . $empresa_str;
            $desc_str = "Disponemos de gran variedad de accesorios, que podras comprar en nuestras instalaciones para poder preparar tu veh&iacute;culo y tu viaje con todo detalle | " . $empresa_str;
            break;
        case 'quienes-somos':
            $title_str = "Quienes somos, conoce nuestra historia | " . $empresa_str;
            $desc_str = "Conoce la historia de Nusa Caravaning, los motivos de su creacion y a las personas que formamos parte de ello | " . $empresa_str;
            break;
        case 'donde-estamos':
            $title_str = "Donde estamos - encuentranos en Valladolid | " . $empresa_str;
            $desc_str = "Localiza nuestras instalaciones en Valladolid ( Villanueva de Duero ), para que puedas venir a visitarnos cuando quieras | " . $empresa_str;
            break;
        case 'contacto':
            $title_str = "Contacto - disponibilidad de alquiler, venta, ocasion y servicio tecnico | " . $empresa_str;
            $desc_str = "Consulta disponibilidad de alquiler de autocaravanas, modelos disponibles en caravanas y autocaravanas nuevas y de ocasion. Pide cita para nuestro servicio tecnico | " . $empresa_str;
            break;
    }   
}
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $title_str; ?></title>
<meta name="title" content="<?php echo $title_str; ?>"/>
<meta name="keywords" content="comprar,venta, caravana, caravanas, weinsberg,fendt,across, valladolid, segovia, salamanca, avila, soria, palencia, leon, zamora"/>
<meta name="description" content="<?php echo $desc_str; ?>" />
<meta property="og:title" content="<?php echo $title_str; ?>" />
<meta property="og:description" content="<?php echo $desc_str; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $url; ?>" />
<meta property="og:image" content="<?php echo $image; ?>" />
<meta name="twitter:title" content="<?php echo $title_str; ?>" />
<meta name="twitter:description" content="<?php echo $title_str; ?>" />
<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="<?php echo $url; ?>">
<meta name="twitter:image" content="<?php echo $image; ?>">
<meta name="language" content="spanish" />
<meta name="distribution" content="global" />
<meta name="author" content="Nusa Caravaning S.L." /> 	

<link rel="canonical" href="http://www.nusacaravaning.com<?php echo $_SERVER['REQUEST_URI']; ?>" />
<link rel="icon" type="image/png" href="/favicon.png">
<link rel="stylesheet" type="text/css" media="screen" href="/css/estilos_responsive.css">

<!-- GOOGLE ANALYTICS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72700526-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- PLUGINS -->
<script src="/js/jquery.min.js" defer></script>
<link href="/css/fotorama.css" rel="stylesheet">
<script src="/js/fotorama.js" defer></script>

<div id="fb-root"></div>
        <script async defer>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.5";
          fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>

<script async defer>
  window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));
</script>

<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'es'}
</script>

