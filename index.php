<head>
  <?php include 'code/head_responsive.php'; ?>
</head>

<body>

 <div id="elem_cent_fondo"></div>
 <div id="elem_cent_contenedor" onclick="closePopup()">
  <div id="elem_cent_item">
  </div>
 </div>

 <div id="contenedor">

  <header id="cabecera">
    <?php include 'code/encabezado_galeria.php'; ?>
  </header>

  <nav id="menu_superior">
    <a href="/"><div id="menu_superior_btn" id="btn_inicio">Inicio</div></a>
    <a href="/quienes-somos"><div id="menu_superior_btn"><h1>Qui&eacute;nes somos</h1></div></a>
    <a href="/donde-estamos"><div id="menu_superior_btn"><h1>D&oacute;nde estamos</h1></div></a>
    <a href="/contacto"><div id="menu_superior_btn"><h1>Contacto</h1></div></a>
  </nav>

  <section id="menu_lateral">
    <?php include 'code/menu_izq.php'; ?>
  </section>

  <section id="contenido">

  <?php
     switch ($_GET["id"]) {
        case 'caravanas': include 'code/caravanas.php';break;
        case 'autocaravanas': include 'code/autocaravanas.php';break;
        case 'stock': include 'code/stock.php';break;
        case 'alquiler-autocaravanas': include 'code/alquiler.php';break;
        case 'ocasion': include 'code/ocasion.php';break;
        case 'instalacion-enganches': include 'code/enganches.php';break;
        case 'servicio-tecnico': include 'code/serviciotecnico.php';break;
        case 'contacto': include 'code/contacto.php';break;
        case 'parking': include 'code/parking.php';break;
        case 'accesorios': include 'code/accesorios.php';break;
        //case 'ofertas': include 'code/ofertas.php';break;
        case 'quienes-somos': include 'code/quienessomos.php';break;
        case 'donde-estamos': include 'code/dondeestamos.php';break;
        case 'pagina-no-encontrada': include '404.php';break;
        default: include 'code/inicio.php';
     }
  ?>

  </section> <!-- fin contenido -->

  <?php include 'code/pie.php'; ?>

 </div> <!-- fin contenedor -->

</body>
