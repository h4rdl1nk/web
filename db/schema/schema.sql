CREATE DATABASE IF NOT EXISTS nusaweb_v2;

USE nusaweb_v2;

/*CREATE TABLE new_tbl AS SELECT * FROM orig_tbl;*/
/*IMPORTANT: column 'nombre' is mandatory for any tabli in this schema*/

DROP TABLE IF EXISTS caravanas_stock_imagen;
DROP TABLE IF EXISTS caravanas_catalogo_imagen;
DROP TABLE IF EXISTS autocaravanas_alquiler_imagen;
DROP TABLE IF EXISTS autocaravanas_stock_imagen;
DROP TABLE IF EXISTS autocaravanas_catalogo_imagen;
DROP TABLE IF EXISTS autocaravanas_gama_imagen;
DROP TABLE IF EXISTS autocaravanas_marca_imagen;
DROP TABLE IF EXISTS imagen_tipo;
DROP TABLE IF EXISTS caravanas_stock;
DROP TABLE IF EXISTS caravanas_catalogo;
DROP TABLE IF EXISTS autocaravanas_alquiler;
DROP TABLE IF EXISTS autocaravanas_stock;
DROP TABLE IF EXISTS autocaravanas_catalogo;
DROP TABLE IF EXISTS autocaravanas_tipo;
DROP TABLE IF EXISTS autocaravanas_gama;
DROP TABLE IF EXISTS autocaravanas_marca;
DROP TABLE IF EXISTS caravanas_gama;
DROP TABLE IF EXISTS caravanas_marca;

CREATE TABLE IF NOT EXISTS caravanas_marca (
	id int UNIQUE AUTO_INCREMENT,
	nombre varchar(20) NOT NULL,
	webpage varchar(120) DEFAULT NULL,
	orden int DEFAULT NULL,
	redirect boolean DEFAULT NULL,
	visible boolean DEFAULT 1,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS caravanas_gama (
	id int UNIQUE AUTO_INCREMENT,
	id_marca int NOT NULL,
	nombre varchar(20) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_marca) REFERENCES caravanas_marca(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_marca (
	id int UNIQUE AUTO_INCREMENT,
	nombre varchar(20) NOT NULL,
	webpage varchar(120) DEFAULT NULL,
	orden int DEFAULT NULL,
	redirect boolean DEFAULT NULL,
	visible boolean DEFAULT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_gama (
	id int UNIQUE AUTO_INCREMENT,
	id_marca int NOT NULL,
	nombre varchar(20) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_marca) REFERENCES autocaravanas_marca(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_tipo (
	id int UNIQUE AUTO_INCREMENT,
	nombre varchar(20) DEFAULT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS caravanas_stock (
	id int UNIQUE AUTO_INCREMENT,
	id_marca int NOT NULL,
	id_gama int DEFAULT NULL,
	visible boolean DEFAULT 1,
	nombre varchar(20) DEFAULT NULL,
	temporada int DEFAULT NULL,
	dim_altura varchar(12) DEFAULT NULL,
	dim_anchura varchar(12) DEFAULT NULL,
	dim_longitud varchar(12) DEFAULT NULL,
	precio int DEFAULT NULL,
	precio_rebajado int DEFAULT NULL,
	peso_tara int DEFAULT NULL,
	peso_mma int DEFAULT NULL,
	motor varchar(20) DEFAULT NULL,
	chasis varchar(20) DEFAULT NULL,
	dim_cama_delantera varchar(12) DEFAULT NULL,
	dim_cama_central varchar(12) DEFAULT NULL,
	dim_cama_trasera varchar(12) DEFAULT NULL,
	dim_cama_literas varchar(12) DEFAULT NULL,
	plazas_viajar int DEFAULT NULL,
	plazas_dormir int DEFAULT NULL,
	descripcion text DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_marca) REFERENCES caravanas_marca(id),
	FOREIGN KEY (id_gama) REFERENCES caravanas_gama(id)
);

CREATE TABLE IF NOT EXISTS caravanas_catalogo (
	id int UNIQUE AUTO_INCREMENT,
	id_marca int NOT NULL,
	id_gama int DEFAULT NULL,
	visible boolean DEFAULT 1,
	nombre varchar(20) DEFAULT NULL,
	temporada int DEFAULT NULL,
	dim_altura varchar(12) DEFAULT NULL,
	dim_anchura varchar(12) DEFAULT NULL,
	dim_longitud varchar(12) DEFAULT NULL,
	precio int DEFAULT NULL,
	precio_rebajado int DEFAULT NULL,
	peso_tara int DEFAULT NULL,
	peso_mma int DEFAULT NULL,
	motor varchar(20) DEFAULT NULL,
	chasis varchar(20) DEFAULT NULL,
	dim_cama_delantera varchar(12) DEFAULT NULL,
	dim_cama_central varchar(12) DEFAULT NULL,
	dim_cama_trasera varchar(12) DEFAULT NULL,
	dim_cama_literas varchar(12) DEFAULT NULL,
	plazas_viajar int DEFAULT NULL,
	plazas_dormir int DEFAULT NULL,
	descripcion text DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_marca) REFERENCES caravanas_marca(id),
	FOREIGN KEY (id_gama) REFERENCES caravanas_gama(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_stock (
	id int UNIQUE AUTO_INCREMENT,
	id_marca int NOT NULL,
	id_gama int DEFAULT NULL,
	id_tipo int NOT NULL,
	visible boolean DEFAULT 1,
	nombre varchar(20) DEFAULT NULL,
	temporada int DEFAULT NULL,
	dim_altura varchar(12) DEFAULT NULL,
	dim_anchura varchar(12) DEFAULT NULL,
	dim_longitud varchar(12) DEFAULT NULL,
	precio int DEFAULT NULL,
	precio_rebajado int DEFAULT NULL,
	peso_tara int DEFAULT NULL,
	peso_mma int DEFAULT NULL,
	motor varchar(20) DEFAULT NULL,
	chasis varchar(20) DEFAULT NULL,
	dim_cama_delantera varchar(12) DEFAULT NULL,
	dim_cama_central varchar(12) DEFAULT NULL,
	dim_cama_trasera varchar(12) DEFAULT NULL,
	dim_cama_literas varchar(12) DEFAULT NULL,
	plazas_viajar int DEFAULT NULL,
	plazas_dormir int DEFAULT NULL,
	descripcion text DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_tipo) REFERENCES autocaravanas_tipo(id),
	FOREIGN KEY (id_marca) REFERENCES autocaravanas_marca(id),
	FOREIGN KEY (id_gama) REFERENCES autocaravanas_gama(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_catalogo (
	id int UNIQUE AUTO_INCREMENT,
	id_marca int NOT NULL,
	id_gama int DEFAULT NULL,
	id_tipo int NOT NULL,
	visible boolean DEFAULT 1,
	nombre varchar(20) DEFAULT NULL,
	temporada int DEFAULT NULL,
	dim_altura varchar(12) DEFAULT NULL,
	dim_anchura varchar(12) DEFAULT NULL,
	dim_longitud varchar(12) DEFAULT NULL,
	precio int DEFAULT NULL,
	precio_rebajado int DEFAULT NULL,
	peso_tara int DEFAULT NULL,
	peso_mma int DEFAULT NULL,
	motor varchar(20) DEFAULT NULL,
	chasis varchar(20) DEFAULT NULL,
	dim_cama_delantera varchar(12) DEFAULT NULL,
	dim_cama_central varchar(12) DEFAULT NULL,
	dim_cama_trasera varchar(12) DEFAULT NULL,
	dim_cama_literas varchar(12) DEFAULT NULL,
	plazas_viajar int DEFAULT NULL,
	plazas_dormir int DEFAULT NULL,
	descripcion text DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_tipo) REFERENCES autocaravanas_tipo(id),
	FOREIGN KEY (id_marca) REFERENCES autocaravanas_marca(id),
	FOREIGN KEY (id_gama) REFERENCES autocaravanas_gama(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_alquiler (
	id int UNIQUE AUTO_INCREMENT,
	id_marca int NOT NULL,
	id_gama int DEFAULT NULL,
	id_tipo int NOT NULL,
	visible boolean DEFAULT 1,
	nombre varchar(20) DEFAULT NULL,
	temporada int DEFAULT NULL,
	dim_altura varchar(12) DEFAULT NULL,
	dim_anchura varchar(12) DEFAULT NULL,
	dim_longitud varchar(12) DEFAULT NULL,
	precio int DEFAULT NULL,
	precio_rebajado int DEFAULT NULL,
	peso_tara int DEFAULT NULL,
	peso_mma int DEFAULT NULL,
	motor varchar(20) DEFAULT NULL,
	chasis varchar(20) DEFAULT NULL,
	dim_cama_delantera varchar(12) DEFAULT NULL,
	dim_cama_central varchar(12) DEFAULT NULL,
	dim_cama_trasera varchar(12) DEFAULT NULL,
	dim_cama_literas varchar(12) DEFAULT NULL,
	plazas_viajar int DEFAULT NULL,
	plazas_dormir int DEFAULT NULL,
	descripcion text DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_tipo) REFERENCES autocaravanas_tipo(id),
	FOREIGN KEY (id_marca) REFERENCES autocaravanas_marca(id),
	FOREIGN KEY (id_gama) REFERENCES autocaravanas_gama(id)
);

CREATE TABLE IF NOT EXISTS ocasion (
	id int UNIQUE AUTO_INCREMENT,
	visible boolean DEFAULT 1,
	nombre varchar(45) DEFAULT NULL,
	temporada int DEFAULT NULL,
	dim_altura varchar(12) DEFAULT NULL,
	dim_anchura varchar(12) DEFAULT NULL,
	dim_longitud varchar(12) DEFAULT NULL,
	precio int DEFAULT NULL,
	precio_rebajado int DEFAULT NULL,
	peso_tara int DEFAULT NULL,
	peso_mma int DEFAULT NULL,
	motor varchar(20) DEFAULT NULL,
	chasis varchar(20) DEFAULT NULL,
	dim_cama_delantera varchar(12) DEFAULT NULL,
	dim_cama_central varchar(12) DEFAULT NULL,
	dim_cama_trasera varchar(12) DEFAULT NULL,
	dim_cama_literas varchar(12) DEFAULT NULL,
	plazas_viajar int DEFAULT NULL,
	plazas_dormir int DEFAULT NULL,
	descripcion text DEFAULT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS imagen_tipo (
	id int UNIQUE AUTO_INCREMENT,
	nombre varchar(20) DEFAULT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS caravanas_marca_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES caravanas_marca(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS caravanas_gama_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES caravanas_gama(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS caravanas_stock_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES caravanas_stock(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS caravanas_catalogo_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES caravanas_catalogo(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_marca_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES autocaravanas_marca(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_gama_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES autocaravanas_gama(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_stock_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES autocaravanas_stock(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_catalogo_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES autocaravanas_catalogo(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS autocaravanas_alquiler_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES autocaravanas_alquiler(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);

CREATE TABLE IF NOT EXISTS ocasion_imagen (
	id int UNIQUE AUTO_INCREMENT,
	id_element int NOT NULL,
	id_tipo int DEFAULT NULL,
	nombre varchar(100) DEFAULT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (id_element) REFERENCES ocasion(id),
	FOREIGN KEY (id_tipo) REFERENCES imagen_tipo(id)
);


/*DYNAMIC CHANGES*/

INSERT INTO imagen_tipo (nombre)
		VALUES ('principal'),
			   ('planta-dia'),
			   ('planta-noche'),
			   ('galeria'),
			   ('logo'),
			   ('boton'),
			   ('cabecera'),
			   ('auxiliar-1'),
			   ('auxiliar-2')
			   ('auxiliar-3'),
			   ('auxiliar-4');

INSERT INTO autocaravanas_marca (nombre,webpage) 
	   VALUES ('pilote','https://www.pilote.es'),
	   		  ('carado','https://carado.de/es'),
	          ('pla','http://www.placamper.it/es/'),
	          ('giottiline','http://www.giottiline.com/'),
	          ('etrusco','http://www.etrusco.com/?lang=es'),
	          ('burstner','https://www.burstner.com/es.html'),
	          ('weinsberg','https://www.weinsberg.com/en-int/home.html');

INSERT INTO autocaravanas_gama (nombre,id_marca) 
	   VALUES ('G-LINE','4'),
	   		  ('therry','4'),
	          ('mister','3'),
	          ('happy','3'),
	          ('plasy','3'),
	          ('sandy','3'),
	          ('carabus','7');

INSERT INTO autocaravanas_tipo (nombre) 
	   VALUES ('perfilado'),
	   		  ('capuchino'),
	   		  ('integral'),
	   		  ('van');

INSERT INTO autocaravanas_stock (id_marca,id_gama,id_tipo,nombre) 
	   VALUES ('3','5','2','P70G'),
	   		  ('3','4','2','435'),
	          ('3','4','2','440'),
	          ('3','3','1','550'),
	          ('3','3','1','560'),
	          ('3','3','1','570'),
	          ('3','6','3','S74'),
	          ('3','5','1','HP74'),
	          ('4','2','2','T45'),
	          ('4','2','1','T38'),
	          ('4','2','1','T36'),
	          ('4','1','3','938'),
	          ('7','7','4','601K');

ALTER TABLE caravanas_stock 
	MODIFY dim_altura varchar(12),
	MODIFY dim_anchura varchar(12),
	MODIFY dim_longitud varchar(12),
	MODIFY dim_cama_delantera varchar(24),
	MODIFY dim_cama_central varchar(24),
	MODIFY dim_cama_trasera varchar(24),
	MODIFY dim_cama_literas varchar(24);

ALTER TABLE autocaravanas_stock 
	MODIFY dim_altura varchar(12),
	MODIFY dim_anchura varchar(12),
	MODIFY dim_longitud varchar(12),
	MODIFY dim_cama_delantera varchar(24),
	MODIFY dim_cama_central varchar(24),
	MODIFY dim_cama_trasera varchar(24),
	MODIFY dim_cama_literas varchar(24);

ALTER TABLE autocaravanas_catalogo 
	MODIFY dim_altura varchar(12),
	MODIFY dim_anchura varchar(12),
	MODIFY dim_longitud varchar(12),
	MODIFY dim_cama_delantera varchar(24),
	MODIFY dim_cama_central varchar(24),
	MODIFY dim_cama_trasera varchar(24),
	MODIFY dim_cama_literas varchar(24);

ALTER TABLE autocaravanas_alquiler 
	MODIFY dim_altura varchar(12),
	MODIFY dim_anchura varchar(12),
	MODIFY dim_longitud varchar(12),
	MODIFY dim_cama_delantera varchar(24),
	MODIFY dim_cama_central varchar(24),
	MODIFY dim_cama_trasera varchar(24),
	MODIFY dim_cama_literas varchar(24);

/*20180115*/
ALTER TABLE autocaravanas_marca ADD visible boolean DEFAULT 1;
ALTER TABLE caravanas_marca ADD visible boolean DEFAULT 1;
ALTER TABLE autocaravanas_alquiler ADD visible boolean DEFAULT 1;
ALTER TABLE autocaravanas_stock ADD visible boolean DEFAULT 1;
ALTER TABLE autocaravanas_catalogo ADD visible boolean DEFAULT 1;
ALTER TABLE caravanas_catalogo ADD visible boolean DEFAULT 1;
ALTER TABLE caravanas_stock ADD visible boolean DEFAULT 1;