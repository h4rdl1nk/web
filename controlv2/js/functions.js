
function deleteFileAlert($path,$table,$id){
    var httpreq = new XMLHttpRequest();
    httpreq.onreadystatechange = function() {
        if (httpreq.readyState == XMLHttpRequest.DONE) {
            alert("Resultado: " + httpreq.responseText);
        }
    }
    httpreq.open("GET","/controlv2/actions.php?action=imageDelete&path=" + $path + "&table=" + $table + "&id=" + $id, true);   
    httpreq.send();
    console.log('OPENED', httpreq.status);
}

function formFilesUpload($folder){
    var form = $('#fileUploadForm')[0];
    var data = new FormData(form);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "actions.php?action=imagesUpload&folder=" + $folder,
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (returndata) {
            alert(returndata);
        },
        error: function (e) {
            $("#result").text(e.responseText);
                console.log("ERROR : ", e);
            $("#btnSubmit").prop("disabled", false);
        }
    });

}

function tableFormSend(){
        var elements = document.getElementById("addreg").elements;
        var obj = {
            "table" : "",
            "action" : "",
            "id" : "",
            "fields" : []
        };
        for (var i = 0, element; element = elements[i++];) {
            if(element.type != 'button'){
                switch(element.name){
                    case "table":
                        obj.table = element.value;
                        break;
                    case "action":
                        obj.action = element.value;
                        break;
                    case "id":
                        obj.id = element.value;
                        break;
                    default:
                        var field = {};
                        field['name'] = element.name;
                        field['value'] = element.value;
                        obj.fields.push(field);
                }
            }
        }
        var obj_json = JSON.stringify(obj);
        var httpreq = new XMLHttpRequest();
        httpreq.onreadystatechange = function() {
            if (httpreq.readyState == XMLHttpRequest.DONE) {
                alert("Resultado: " + httpreq.responseText);
                document.getElementById("addreg").reset();
            }
        }
        httpreq.open("POST","/controlv2/dbexec.php", true);	
        httpreq.setRequestHeader("Content-type", "application/json");	
        httpreq.send(obj_json);
        console.log('OPENED', httpreq.status);
};

function deleteAlert(table,id){
        var obj = {
            "table" : "",
            "action" : "",
            "id" : ""
        };
        obj.table = table;
        obj.action = "delete";
        obj.id = id;

        var obj_json = JSON.stringify(obj);
        var httpreq = new XMLHttpRequest();
        httpreq.onreadystatechange = function() {
            if (httpreq.readyState == XMLHttpRequest.DONE) {
                alert("Resultado: " + httpreq.responseText);
                document.getElementById("addreg").reset();
            }
        }
        httpreq.open("POST","/controlv2/dbexec.php", true);	
        httpreq.setRequestHeader("Content-type", "application/json");	
        httpreq.send(obj_json);
        console.log('OPENED', httpreq.status);      
}
