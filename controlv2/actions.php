<?php

require_once(realpath('include/functions.php'));
require_once(realpath('config/db.php'));

$db_schema = $config['db']['v2']['dbname'];
$db_user = $config['db']['v2']['users']['rw']['username'];
$db_pass = $config['db']['v2']['users']['rw']['password'];
$db_host = $config['db']['v2']['host'];

$conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
if ($mysqli->connect_errno) {
    echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

switch($_GET['action']){
	case "add":
		tableToForm($conn,$_GET['table']);
		break;
	case "edit":
        tableToForm($conn,$_GET['table'],$_GET['id']);
        break;
	case "list":
		tableList($conn,$config,$_GET['table']);	
		break;
	case "delete":
		tableDelete($conn,$_GET['table'],$_GET['id']);	
		break;
	case "images":
		editImages($config,$_GET['table'],$_GET['id'],$conn);
		break;
	case "imagesUploadDir":
		uploadImagesDir($_GET['folder'],$_FILES,$_GET['table'],$_GET['id'],$conn);
		break;
	case "imageDelete":
		deleteImage($_GET['path'],$_GET['table'],$_GET['id'],$conn);
		break;
	default:
		echo "Error en parametro action: " . $_GET['action'];
}

$conn->close();

?>
