<HEAD>

<script type="text/javascript" src="js/functions.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/styles.css">

<script type="text/javascript">
        $(document).ready(
            function(){
                   
            }
        );
    </script>

</HEAD>

<DIV ID="button_container">
    <DIV CLASS="button_group">
        <INPUT TYPE="button" VALUE="Nueva marca autocaravana" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=autocaravanas_marca');"/>
        <INPUT TYPE="button" VALUE="Nueva gama autocaravana" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=autocaravanas_gama');"/>
        <INPUT TYPE="button" VALUE="Nueva autocaravana stock" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=autocaravanas_stock');"/>
        <INPUT TYPE="button" VALUE="Nueva autocaravana catalogo" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=autocaravanas_catalogo');"/>
        <INPUT TYPE="button" VALUE="Nueva autocaravana alquiler" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=autocaravanas_alquiler');"/>
    </DIV>
    <DIV CLASS="button_group">
        <INPUT TYPE="button" VALUE="Listar marcas autocaravana" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=autocaravanas_marca');"/>
        <INPUT TYPE="button" VALUE="Listar gamas autocaravana" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=autocaravanas_gama');"/>
        <INPUT TYPE="button" VALUE="Listar autocaravanas stock" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=autocaravanas_stock');"/>
        <INPUT TYPE="button" VALUE="Listar autocaravanas catalogo" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=autocaravanas_catalogo');"/>
        <INPUT TYPE="button" VALUE="Listar autocaravanas alquiler" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=autocaravanas_alquiler');"/>
    </DIV>
    <DIV CLASS="button_group">
        <INPUT TYPE="button" VALUE="Nueva marca caravana" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=caravanas_marca');"/>
        <INPUT TYPE="button" VALUE="Nueva gama caravana" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=caravanas_gama');"/>
        <INPUT TYPE="button" VALUE="Nueva caravana stock" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=caravanas_stock');"/>
        <INPUT TYPE="button" VALUE="Nueva caravana catalogo" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=caravanas_catalogo');"/>
    </DIV>
    <DIV CLASS="button_group">
        <INPUT TYPE="button" VALUE="Listar marcas caravana" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=caravanas_marca');"/>
        <INPUT TYPE="button" VALUE="Listar gamas caravana" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=caravanas_gama');"/>
        <INPUT TYPE="button" VALUE="Listar caravanas stock" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=caravanas_stock');"/>
        <INPUT TYPE="button" VALUE="Listar caravanas catalogo" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=caravanas_catalogo');"/>
    </DIV>
    <DIV CLASS="button_group">
        <INPUT TYPE="button" VALUE="Listar ocasion" 
            ONCLICK="$('#accion').load('actions.php?action=list&table=ocasion');"/>
        <INPUT TYPE="button" VALUE="Nueva ocasion" 
            ONCLICK="$('#accion').load('actions.php?action=add&table=ocasion');"/>
    </DIV>
</DIV>

<DIV ID="accion">
</DIV>
