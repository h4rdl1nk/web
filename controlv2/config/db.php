<?php

$config = array(
    "images" => array(
        "rootDir" => "static/img",
        "elements" => array(
            "rootDir" => "elements"
        ),
        "autocaravanas_marca" => array(
            "gallery" => false,
            "rootDir" => "autocaravanas-marca",
            "imgTable" => "autocaravanas_marca_imagen"
        ),
        "autocaravanas_gama" => array(
            "gallery" => false,
            "rootDir" => "autocaravanas-gama",
            "imgTable" => "autocaravanas_gama_imagen"
        ),
        "autocaravanas_stock" => array(
            "gallery" => true,
            "rootDir" => "autocaravanas-stock",
            "imgTable" => "autocaravanas_stock_imagen"
        ),
        "autocaravanas_catalogo" => array(
            "gallery" => true,
            "rootDir" => "autocaravanas-catalogo",
            "imgTable" => "autocaravanas_catalogo_imagen"
        ),
        "autocaravanas_alquiler" => array(
            "gallery" => true,
            "rootDir" => "autocaravanas-alquiler",
            "imgTable" => "autocaravanas_alquiler_imagen"
        ),
        "caravanas_marca" => array(
            "gallery" => false,
            "rootDir" => "caravanas-marca",
            "imgTable" => "caravanas_marca_imagen"
        ),
        "caravanas_gama" => array(
            "gallery" => false,
            "rootDir" => "caravanas-gama",
            "imgTable" => "caravanas_gama_imagen"
        ),
        "caravanas_stock" => array(
            "gallery" => true,
            "rootDir" => "caravanas-stock",
            "imgTable" => "caravanas_stock_imagen"
        ),
        "caravanas_catalogo" => array(
            "gallery" => true,
            "rootDir" => "caravanas-catalogo",
            "imgTable" => "caravanas_catalogo_imagen"
        ),
        "ocasion" => array(
            "gallery" => true,
            "rootDir" => "ocasion",
            "imgTable" => "ocasion_imagen"
        )
    ),
    "db" => array(
        "v2" => array(
            "dbname" => "nusaweb_v2",
            "users" => array(
                "rw" => array(
                    "username" => "controlUser",
                    "password" => "2o0c0tr_Lp4S!oCT"        
                ),
                "ro" => array(
                    "username" => "appUser",
                    "password" => "r34DusS3NrUs4w3B"
                )
            ),
            "host" => "localhost",
            "tables" => array(
                "caravanas_marca" => array(
                    "list_query" => "SELECT T.id AS id,T.orden AS orden,T.visible AS visible,T.nombre AS nombre,T.webpage AS web FROM caravanas_marca T ORDER BY orden ASC"
                ),
                "caravanas_gama" => array(
                    "list_query" => "SELECT T.id AS id,T.nombre AS nombre,M.nombre AS marca 
                                     FROM caravanas_gama T JOIN caravanas_marca M
                                     WHERE T.id_marca = M.id"
                ),
                "caravanas_stock" => array(
                    "detail_query" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama
                                        FROM caravanas_stock a 
                                        JOIN caravanas_marca m 
                                        JOIN caravanas_gama g
                                        WHERE a.id_marca = m.id 
                                        AND a.id_gama = g.id",
                    "detail_query_marca" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama
                                        FROM caravanas_stock a 
                                        JOIN caravanas_marca m 
                                        JOIN caravanas_gama g 
                                        WHERE a.id_marca = m.id 
                                        AND a.id_gama = g.id 
                                        AND m.nombre = '?'",
                    "list_query" => "SELECT a.id AS id,a.nombre AS nombre,a.temporada AS temporada,m.nombre AS marca,g.nombre AS gama
                                     FROM caravanas_stock a 
                                     JOIN caravanas_gama g 
                                     JOIN caravanas_marca m 
                                     WHERE a.id_gama = g.id 
                                     AND a.id_marca = m.id 
                                     ORDER BY m.id,g.id,a.id"
                ),
                "caravanas_catalogo" => array(
                    "detail_query" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama
                                        FROM caravanas_catalogo a 
                                        JOIN caravanas_marca m 
                                        JOIN caravanas_gama g
                                        WHERE a.id_marca = m.id 
                                        AND a.id_gama = g.id",
                    "detail_query_marca" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama
                                        FROM caravanas_catalogo a 
                                        JOIN caravanas_marca m 
                                        JOIN caravanas_gama g 
                                        WHERE a.id_marca = m.id 
                                        AND a.id_gama = g.id 
                                        AND m.nombre = '?'",
                    "list_query" => "SELECT a.id AS id,a.nombre AS nombre,a.temporada AS temporada,m.nombre AS marca,g.nombre AS gama
                                     FROM caravanas_catalogo a 
                                     JOIN caravanas_gama g 
                                     JOIN caravanas_marca m 
                                     WHERE a.id_gama = g.id 
                                     AND a.id_marca = m.id 
                                     ORDER BY m.id,g.id,a.id"
                ),
                "autocaravanas_marca" => array(
                    "list_query" => "SELECT T.id AS id,T.orden AS orden,T.visible AS visible,T.nombre AS nombre,T.webpage AS web FROM autocaravanas_marca T ORDER BY orden ASC"
                ),
                "autocaravanas_gama" => array(
                    "list_query" => "SELECT T.id AS id,T.nombre AS nombre,M.nombre AS marca 
                                     FROM autocaravanas_gama T JOIN autocaravanas_marca M
                                     WHERE T.id_marca = M.id"
                ),
                "autocaravanas_stock" => array(
                    "detail_query" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama,t.nombre AS tipo
                                        FROM autocaravanas_stock a 
                                        JOIN autocaravanas_marca m 
                                        JOIN autocaravanas_gama g 
                                        JOIN autocaravanas_tipo t 
                                        WHERE a.id_tipo = t.id 
                                        AND a.id_marca = m.id 
                                        AND a.id_gama = g.id",
                    "detail_query_marca" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama,t.nombre AS tipo
                                        FROM autocaravanas_stock a 
                                        JOIN autocaravanas_marca m 
                                        JOIN autocaravanas_gama g 
                                        JOIN autocaravanas_tipo t 
                                        WHERE a.id_tipo = t.id 
                                        AND a.id_marca = m.id 
                                        AND a.id_gama = g.id 
                                        AND m.nombre = '?'",
                    "list_query" => "SELECT a.id AS id,a.nombre AS nombre,a.temporada AS temporada,m.nombre AS marca,g.nombre AS gama,at.nombre AS tipo 
                                     FROM autocaravanas_stock a 
                                     JOIN autocaravanas_gama g 
                                     JOIN autocaravanas_marca m 
                                     JOIN autocaravanas_tipo at
                                     WHERE a.id_gama = g.id 
                                     AND a.id_marca = m.id 
                                     AND a.id_tipo = at.id
                                     ORDER BY m.id,g.id,at.id,a.id"
                ),
                "autocaravanas_catalogo" => array(
                    "detail_query_marca" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama,t.nombre AS tipo
                                        FROM autocaravanas_catalogo a 
                                        JOIN autocaravanas_marca m 
                                        JOIN autocaravanas_gama g 
                                        JOIN autocaravanas_tipo t 
                                        WHERE a.id_tipo = t.id 
                                        AND a.id_marca = m.id 
                                        AND a.id_gama = g.id 
                                        AND m.nombre = '?'",
                    "list_query" => "SELECT a.id AS id,a.nombre AS nombre,a.temporada AS temporada,m.nombre AS marca,g.nombre AS gama,at.nombre AS tipo 
                                     FROM autocaravanas_catalogo a 
                                     JOIN autocaravanas_gama g 
                                     JOIN autocaravanas_marca m 
                                     JOIN autocaravanas_tipo at
                                     WHERE a.id_gama = g.id 
                                     AND a.id_marca = m.id 
                                     AND a.id_tipo = at.id
                                     ORDER BY m.id,g.id,at.id,a.id"
                ),
                "autocaravanas_alquiler" => array(
                    "detail_query" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama,t.nombre AS tipo
                                        FROM autocaravanas_alquiler a 
                                        JOIN autocaravanas_marca m 
                                        JOIN autocaravanas_gama g 
                                        JOIN autocaravanas_tipo t 
                                        WHERE a.id_tipo = t.id 
                                        AND a.id_marca = m.id 
                                        AND a.id_gama = g.id",
                    "detail_query_marca" => "SELECT a.id AS id,a.visible AS visible,a.temporada AS temporada,m.id as id_marca,g.id as id_gama,a.nombre AS nombre,m.nombre AS marca,g.nombre AS gama,t.nombre AS tipo
                                        FROM autocaravanas_alquiler a 
                                        JOIN autocaravanas_marca m 
                                        JOIN autocaravanas_gama g 
                                        JOIN autocaravanas_tipo t 
                                        WHERE a.id_tipo = t.id 
                                        AND a.id_marca = m.id 
                                        AND a.id_gama = g.id 
                                        AND m.nombre = '?'",
                    "list_query" => "SELECT a.id AS id,a.visible AS visible,a.nombre AS nombre,a.temporada AS temporada,m.nombre AS marca,g.nombre AS gama,at.nombre AS tipo 
                                     FROM autocaravanas_alquiler a 
                                     JOIN autocaravanas_gama g 
                                     JOIN autocaravanas_marca m 
                                     JOIN autocaravanas_tipo at
                                     WHERE a.id_gama = g.id 
                                     AND a.id_marca = m.id 
                                     AND a.id_tipo = at.id
                                     ORDER BY m.id,g.id,at.id,a.id"
                ),
                "ocasion" => array(
                    "detail_query" => "SELECT o.id AS id,o.visible AS visible,o.temporada AS temporada,o.nombre AS nombre
                                       FROM ocasion o",
                    "list_query" => "SELECT o.id AS id,o.nombre AS nombre,o.visible AS visible,o.temporada AS temporada
                                     FROM ocasion o
                                     ORDER BY o.id,o.nombre"
                )
            )
        )
    )
);

