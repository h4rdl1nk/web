<?php 

function deleteImage($path,$table,$id,$conn){

    if(isset($id)){
        $query_delete = "DELETE FROM " . $table . " WHERE id = '" . $id . "'";

        if ($res_delete = $conn->query($query_delete)){
            echo 'Imagen borrada de BD [' . $table . '/' . $id . ']';
        }
    }

	if(unlink($path)){
		echo 'Imagen borrada: ' . $path;
	}
	else{
		echo 'Error: ' . $path;
	}
}

function uploadImagesDir($folder,$images,$table,$id,$conn){

	$uploadType = "";

    //DB settings
    $imgTable = $table . '_imagen';
    $elementId = $id;
    /*$query = "SELECT nombre 
              FROM " . $imgTable . " 
              WHERE id_element = '' 
              AND id_tipo = (SELECT id FROM imagen_tipo WHERE nombre = '" .  . "');";
    */

	if(sizeof($images['imageFiles']) > 0){
		$uploadType = "gallery";
	}
	else{
		$uploadType = "single";
	}

	switch($uploadType){
		case "single":
			$numFiles = 0;

            if(is_array($images['imageAuxiliar1']) && sizeof( $images['imageAuxiliar1'] > 0)){
                $ext = explode('/',$images['imageAuxiliar1']['type']);
                if(move_uploaded_file($images['imageAuxiliar1']['tmp_name'],$folder . '/' . $images['imageAuxiliar1']['name'])){
                    $numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageAuxiliar1']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'auxiliar-1'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen auxiliar 1. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen auxiliar 1. DB: ERROR';
                    }
                }
            }
            if(is_array($images['imageAuxiliar2']) && sizeof( $images['imageAuxiliar2'] > 0)){
                $ext = explode('/',$images['imageAuxiliar2']['type']);
                if(move_uploaded_file($images['imageAuxiliar2']['tmp_name'],$folder . '/' . $images['imageAuxiliar2']['name'])){
                    $numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageAuxiliar2']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'auxiliar-2'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen auxiliar 2. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen auxiliar 2. DB: ERROR';
                    }
                }
            }
			if(is_array($images['imageAuxiliar3']) && sizeof( $images['imageAuxiliar3'] > 0)){
				$ext = explode('/',$images['imageAuxiliar3']['type']);
				if(move_uploaded_file($images['imageAuxiliar3']['tmp_name'],$folder . '/' . $images['imageAuxiliar3']['name'])){
					$numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageAuxiliar3']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'auxiliar-3'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen auxiliar 3. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen auxiliar 3. DB: ERROR';
                    }
                }
			}
            if(is_array($images['imageAuxiliar4']) && sizeof( $images['imageAuxiliar4'] > 0)){
                $ext = explode('/',$images['imageAuxiliar4']['type']);
                if(move_uploaded_file($images['imageAuxiliar4']['tmp_name'],$folder . '/' . $images['imageAuxiliar4']['name'])){
                    $numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageAuxiliar4']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'auxiliar-4'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen auxiliar 4. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen auxiliar-4. DB: ERROR';
                    }
                }
            }
			if(is_array($images['imagePrincipal']) && sizeof( $images['imagePrincipal'] > 0)){
				$ext = explode('/',$images['imagePrincipal']['type']);
				if(move_uploaded_file($images['imagePrincipal']['tmp_name'],$folder . '/' . $images['imagePrincipal']['name'])){
					$numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imagePrincipal']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'principal'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen principal. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen principal. DB: ERROR';
                    }
                }
			}
			if(is_array($images['imageCabecera']) && sizeof( $images['imageCabecera'] > 0)){
				$ext = explode('/',$images['imageCabecera']['type']);
				if(move_uploaded_file($images['imageCabecera']['tmp_name'],$folder . '/' . $images['imageCabecera']['name'])){
					$numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageCabecera']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'cabecera'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen cabecera. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen cabecera. DB: ERROR';
                    }
                }
			}
			if(is_array($images['imageLogo']) && sizeof( $images['imageLogo'] > 0)){
				$ext = explode('/',$images['imageLogo']['type']);
				if(move_uploaded_file($images['imageLogo']['tmp_name'],$folder . '/' . $images['imageLogo']['name'])){
					$numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageLogo']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'logo'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen logo. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen logo. DB: ERROR';
                    }
				}
			}
			if(is_array($images['imageBoton']) && sizeof( $images['imageBoton'] > 0)){
				$ext = explode('/',$images['imageBoton']['type']);
				if(move_uploaded_file($images['imageBoton']['tmp_name'],$folder . '/' . $images['imageBoton']['name'])){
					$numFiles++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageBoton']['name'] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'boton'";

                    if ($res_insert = $conn->query($query_insert)){
                        echo 'Subido fichero imagen boton. DB: OK';
                    }
                    else{
                        echo 'Subido fichero imagen boton. DB: ERROR';
                    }
				}
			}
			if($numFiles < 1){
				echo 'No se han subido ficheros';
			}
		break;

		case "gallery":
			$numFiles = sizeof($images['imageFiles']['name']);
			$numOk = 0;
			$numEr = 0;
            $numOkDb = 0;
            $numErDb = 0;


			for ( $i = 0; $i < $numFiles; $i++){
					/*echo 'Subiendo fichero ' 
						. $images['imageFiles']['tmp_name'][$i] 
						. '->' . $images['imageFiles']['name'][$i]
						. ' [' . $images['imageFiles']['size'][$i] . ']'
						. ' [' . $images['imageFiles']['type'][$i] . '] ... ';*/

				if(move_uploaded_file($images['imageFiles']['tmp_name'][$i],$folder . '/' . $images['imageFiles']['name'][$i])){
					$numOk++;
                    $query_insert = "INSERT INTO " . $imgTable . " 
                                     (id_element,id_tipo,nombre) 
                                     SELECT '" . $id . "',id,'" . $images['imageFiles']['name'][$i] . "' 
                                     FROM imagen_tipo 
                                     WHERE nombre = 'galeria'";

                    if ($res_insert = $conn->query($query_insert)){
                        $numOkDb++;
                    }
                    else{
                        $numErDb++;
                    }
				}
				else{
					$numEr++;
				}
			}
				echo 'Ficheros galeria UP:' . $numOk .'/' . $numEr . ' DB: ' . $numOkDb . '/' . $numErDb;
		break;

		default:
			echo 'Tipo de subida desconocido o no se han enviado ficheros';
	}
}

function editImages($config,$table,$id,$conn){
	$imagesRootDir = $config['images']['rootDir'];
	$tableDir = $config['images'][$table]['rootDir'];
	//$tableImages = $config['images'][$table]['imgTable'];
    $imgTable = $table . '_imagen';
    $elementId = $id;

	$workDir = $_SERVER['DOCUMENT_ROOT'] . "/" . $imagesRootDir . "/" . $tableDir . "/" . $id;


    $query_list = "SELECT id,nombre 
                   FROM " . $imgTable . " 
                   WHERE id_element = '" . $id . "' AND id_tipo = 
                   (SELECT id FROM imagen_tipo WHERE nombre = 'galeria')";

    $query_list_other = "SELECT i.id AS id,t.nombre AS tipo,i.nombre AS nombre 
                     FROM " . $imgTable . " i 
                     JOIN imagen_tipo t 
                     WHERE i.id_tipo = t.id
                     AND i.id_element = '" . $id . "' 
                     AND t.nombre != 'galeria';";

	if (!file_exists($workDir)) {
    	if(mkdir($workDir, 0755, true)){
    		echo "<br>Creado directorio";
    	}
    	else{
    		echo "<br>Error creando directorio!!";
    	}
	}

	//Escanear tabla de imagenes
	?>
	<TABLE>
	<?php
    if ($config['images'][$table]['gallery']){
        if ($res_list = $conn->query($query_list)){
            ?>
            <TR><TD COLSPAN="3">IMAGENES GALERIA
            <?php
            while ($row_list = mysqli_fetch_array($res_list, MYSQLI_ASSOC)){
                ?>
                <TR>
                    <TD>
                        <IMG STYLE="max-height:50px;" SRC="/<?php echo $imagesRootDir . '/' . $tableDir . '/' . $id . '/' . $row_list['nombre']; ?>"></IMG>
                    <TD><?php echo $row_list['nombre']; ?>
                    <TD>
                        <INPUT type="button" value="borrar" onclick="deleteFileAlert('<?php echo $workDir . '/' . $row_list['nombre']; ?>','<?php echo $imgTable; ?>','<?php echo $row_list['id'] ?>');$(this).closest('tr').remove();"/>
                <?php
            }
        }
    }
    ?>
    </TABLE>
    <TABLE>
    <?php
    if ($res_list_other = $conn->query($query_list_other)){
        ?>
        <TR><TD COLSPAN="4">OTRAS IMAGENES
        <?php
        while ($row_list_other = mysqli_fetch_array($res_list_other, MYSQLI_ASSOC)){
            ?>
                <TR>
                    <TD>
                        <IMG STYLE="max-height:50px;" SRC="/<?php echo $imagesRootDir . '/' . $tableDir . '/' . $id . '/' . $row_list_other['nombre']; ?>"></IMG>
                    <TD><?php echo $row_list_other['tipo']; ?>
                    <TD><?php echo $row_list_other['nombre']; ?>
                    <TD>
                        <INPUT type="button" value="borrar" onclick="deleteFileAlert('<?php echo $workDir . '/' . $row_list_other['nombre']; ?>','<?php echo $imgTable; ?>','<?php echo $row_list_other['id'] ?>');$(this).closest('tr').remove();"/>
            <?php
        }
    }
	?>

	</TABLE>
	<?php

	echo "<br>Editando imagenes en directorio: [" . $_SERVER['DOCUMENT_ROOT'] . "/" . $imagesRootDir . "/" . $tableDir . "/" . $id . "]";
    echo "<br>Tabla BD: " . $imgTable;
    ?>
    <ul>
    	<li>Marcas
    		<ul>
    			<li>logo
    			<li>cabecera
    		</ul>
    	<li>Gamas
    		<ul>
    			<li>logo
    			<li>cabecera
    		</ul>
    	<li>Autocaravanas
    		<ul>
    			<li>principal
    			<li>planta
    		</ul>
    </ul>
    <br>

    <script type="text/javascript">
	    $(document).ready(
	        function(){
	        	var max_fields = 15;
				var wrapper = $(".image_fields_wrap");
				var add_button = $(".addField");
				var send_gallery_button = $(".sendFilesGallery");
				var send_files_button = $(".sendFiles");
				var del_file_button = $(".imgDelete");
				var x = 1;

	            $(add_button).click(
	            	function(e){
	            		if(x < max_fields){
	            			x++;
	            			$(wrapper).append('<br>Fichero ' + x + ': <input name="imageFiles[]" type="file" />');
	            		}
	            	}
	            );
	            $(send_files_button).click(
	            	function(){
	            		    var form = $('#fileUploadForm')[0];
        					var data = new FormData(form);

							$.ajax({
            					type: "POST",
            					enctype: 'multipart/form-data',
            					url: "actions.php?action=imagesUploadDir&folder=<?php echo $workDir; ?>&table=<?php echo $table; ?>&id=<?php echo $id; ?>",
            					data: data,
            					processData: false,
            					contentType: false,
            					cache: false,
            					timeout: 600000,
            					success: function (returndata) {
      								alert(returndata);
      								form.reset();
    							},
            					error: function (e) {
                					$("#result").text(e.responseText);
                					console.log("ERROR : ", e);
                					$("#btnSubmit").prop("disabled", false);
                					form.reset();
            					}
        					});
	            	}
	            );
	            $(send_gallery_button).click(
	            	function(){
	            		    var form = $('#galleryUploadForm')[0];
        					var data = new FormData(form);

							$.ajax({
            					type: "POST",
            					enctype: 'multipart/form-data',
            					url: "actions.php?action=imagesUploadDir&folder=<?php echo $workDir; ?>&table=<?php echo $table; ?>&id=<?php echo $id; ?>",
            					data: data,
            					processData: false,
            					contentType: false,
            					cache: false,
            					timeout: 600000,
            					success: function (returndata) {
      								alert(returndata);
      								form.reset();
    							},
            					error: function (e) {
                					$("#result").text(e.responseText);
                					console.log("ERROR : ", e);
                					$("#btnSubmit").prop("disabled", false);
                					form.reset();
            					}
        					});
	            	}
	            );
	        }
	    );
	</script>

  <?php if ($config['images'][$table]['gallery']){ ?>
    <img style="border:2px solid black;margin:10px;" src="/controlv2/static/recuadro_ayuda.jpg"/>
  <?php } ?>
  
	<form action="" enctype="multipart/form-data" id="fileUploadForm" method="POST">
		<table>
        <tr><td colspan="2">SUBIDA IMAGENES AUXILIARES
        <?php if ($config['images'][$table]['gallery']){ ?>
		<!--<tr><td>Imagen principal: <td><input name="imagePrincipal" type="file" />-->
		<tr><td>Imagen auxiliar 1: <td><input name="imageAuxiliar1" type="file" />
        <tr><td>Imagen auxiliar 2: <td><input name="imageAuxiliar2" type="file" />
        <tr><td>Imagen auxiliar 3: <td><input name="imageAuxiliar3" type="file" />
        <tr><td>Imagen auxiliar 4: <td><input name="imageAuxiliar4" type="file" />
        <?php } ?>
        <?php if (!$config['images'][$table]['gallery']){ ?>
		<?php if ($table == "autocaravanas_gama" || $table == "caravanas_gama"){ ?>
		<tr><td>Imagen auxiliar 1: <td><input name="imageAuxiliar1" type="file" />
        <tr><td>Imagen auxiliar 2: <td><input name="imageAuxiliar2" type="file" />
		<?php } ?>
		<tr><td>Imagen cabecera: <td><input name="imageCabecera" type="file" />
		<tr><td>Imagen logo: <td><input name="imageLogo" type="file" />
		<tr><td>Imagen boton: <td><input name="imageBoton" type="file" />
        <?php } ?>
		<tr><td colspan="2"><input type="button" class="sendFiles green" value="SUBIR imagenes"/>
				
		</table>
	</form>

    <?php
    if ($config['images'][$table]['gallery']){
    ?>
    <form action="" enctype="multipart/form-data" id="galleryUploadForm" method="POST">
    	<table>
            <tr><td>SUBIDA IMAGENES GALERIA
    		<tr><td>
    		<div class="image_fields_wrap">
    			<br>Fichero 1: <input name="imageFiles[]" type="file" />
			</div>
			<br>
			<tr><td>
				<input type="button" class="addField" value="Agregar imagen" name="addFileInput"/>
				<input type="button" class="sendFilesGallery green" value="SUBIR imagenes"/>
	    </table>
	</form>
    <?php
    }
}

function tableDelete($conn,$table,$id){
	$query_del = "DELETE FROM " . $table . " WHERE id = '" . $id . "'";

	if ($res_del = $conn->query($query_del)) {
		echo "Ejecutada consulta: " . $query_del;
	}
	else{
		echo "Error ejecutando consulta " . $query_del . "[" . $conn->error . "]";
	}
}

function tableList($conn,$config,$table){

	$query_list = $config['db']['v2']['tables'][$table]['list_query'];

	if ($res_list = $conn->query($query_list)) {
		echo '<TABLE>';
		echo '<TR>';
		$fields = $res_list->fetch_fields();
		foreach ($fields as $field) {
			echo '<TD>' . $field->name;
		}
		echo '<TD>ACCIONES';
		while ($row_list = mysqli_fetch_array($res_list, MYSQLI_ASSOC)){
			echo '<TR>';
			foreach ($fields as $field) {
				echo '<TD>' . $row_list[$field->name];
			}
			echo '<TD>';
			?>
			<INPUT TYPE="button" 
			       VALUE="editar" 
			       ONCLICK="$('#accion').load('actions.php?action=edit&table=<?php echo $table; ?>&id=<?php echo $row_list['id']; ?>');"/>
			<INPUT TYPE="button" 
			       VALUE="borrar" 
			       ONCLICK="deleteAlert('<?php echo $table; ?>','<?php echo $row_list['id']; ?>')"/>
			<INPUT TYPE="button" 
			       VALUE="imagenes" 
			       ONCLICK="$('#accion').load('actions.php?action=images&table=<?php echo $table; ?>&id=<?php echo $row_list['id']; ?>');"/>
			<?php
		}
		echo '</TABLE>';
	}
	else{
			echo "Error consultando campos de la tabla " . $table;
	}
}

function tableToForm($conn,$table,$id){

	if(isset($id)){
		$query_id = "SELECT * FROM " . $table . " WHERE id = '" . $id . "'";
	}
	
	$query = "DESCRIBE " . $table;

	if ($res = $conn->query($query)) {	
		echo '<FORM ID="addreg" METHOD="POST" ACTION="">';
		echo '<TABLE>';
		echo '<INPUT TYPE="hidden" NAME="table" VALUE="'.$table.'"/>';
		if(isset($id)){
			$action = "update";
			echo '<INPUT TYPE="hidden" NAME="id" VALUE="'.$id.'"/>';
		}
		else{
			$action = "insert";
		}
		echo '<INPUT TYPE="hidden" NAME="action" VALUE="'.$action.'"/>';
		if (isset($id)){
			$res_id = $conn->query($query_id);
			$row_id = mysqli_fetch_array($res_id, MYSQLI_ASSOC);
		}
		while($row = mysqli_fetch_array($res, MYSQLI_ASSOC)){
			if($row['Key'] != "PRI"){
				//$query_nombre = "SELECT nombre FROM $table WHERE $row['Field'] = $idc";
				if($res_obj = getColumnForeign($conn,'nusaweb_v2',$table,$row['Field'])){
					echo '<TR><TD>'.$row['Field'].'<TD><SELECT NAME="'.$row['Field'].'">';
                    foreach($res_obj['column'] as $idc => $nombre){
						?>
							<OPTION VALUE="<?php echo $idc; ?>" <?php if(isset($id) && $row_id[$row['Field']] == $idc){ echo 'SELECTED'; } ?> >
							<?php echo $idc . ' - ' . $nombre; ?>
							</OPTION>
						<?php
					}
					echo '</SELECT>';
				}
				else{
					switch($row['Type']){
						case 'text':
							//echo '<TR><TD>'.$row['Field'].'<TD><TEXTAREA NAME="'.$row['Field'].'"/>';
							?>
								<TR>
									<TD><?php echo $row['Field']; ?>
									<TD><TEXTAREA ROWS="25" NAME="<?php echo $row['Field']; ?>"><?php if(isset($id)){echo $row_id[$row['Field']];} ?></TEXTAREA>
							<?php
							break;
            			case 'tinyint(1)':
							?>
								<TR>
								<TD><?php echo $row['Field']; ?>
								<TD><SELECT NAME="<?php echo $row['Field']; ?>">
										<OPTION VALUE="0" <?php if(isset($id) && $row_id[$row['Field']] == 0){ echo 'SELECTED'; }?>>NO</OPTION>
										<OPTION VALUE="1" <?php if(isset($id) && $row_id[$row['Field']] == 1){ echo 'SELECTED'; }?>>SI</OPTION>
									</SELECT>
							<?php
              				break;
						default:
							//echo '<TR><TD>'.$row['Field'].'<TD><INPUT TYPE="text" NAME="'.$row['Field'].'"/>';
							?>
								<TR>
									<TD><?php echo $row['Field']; ?>
									<TD><INPUT TYPE="text" NAME="<?php echo $row['Field']; ?>" 
										<?php 
											if(isset($id)){echo 'VALUE="' . $row_id[$row['Field']] . '"';} 
										?>/>
							<?php
							break;
					}
				}
			}
		}	
		echo '<TR><TD COLSPAN="2"><INPUT TYPE="button" VALUE="ENVIAR" ONCLICK="tableFormSend();">';
		echo '</TABLE>';
		echo '</FORM>';
	}

}


function getColumnForeign($conn,$schema,$table,$column){

	$query = "SELECT TABLE_NAME,COLUMN_NAME,REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
                  FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                  WHERE REFERENCED_TABLE_SCHEMA = '" . $schema . "'
                  AND TABLE_NAME = '" . $table . "'
                  AND COLUMN_NAME = '" . $column . "'";

	$obj = array(
		"table" => "",
		"column" => array()
	);

	if ($res = $conn->query($query)) {
		if ($res->num_rows > 0){
			$row = mysqli_fetch_array($res, MYSQLI_ASSOC);
			$obj['table'] = $row['REFERENCED_COLUMN_NAME'];
			$query_table = "SELECT " . $row['REFERENCED_COLUMN_NAME'] . " as id,nombre as nom FROM " . $row['REFERENCED_TABLE_NAME'];
			$obj['table'] = $row['REFERENCED_TABLE_NAME'];
			if ($res_table = $conn->query($query_table)) {	
				while($row_table = mysqli_fetch_array($res_table, MYSQLI_ASSOC)){	
					$obj['column'][$row_table['id']] = $row_table['nom'];
				}
			}
			return $obj;
		}
		else{
			return false;
		}
        }

}

?>
