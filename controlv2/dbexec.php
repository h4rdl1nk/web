<?php

require_once(realpath('config/db.php'));

$json_string = file_get_contents('php://input');

$json_obj = json_decode($json_string);

switch($json_obj->action){
	case "insert":
		$fields = [];
		$values = [];
		foreach( $json_obj->fields as $field ){
			$fields[] = $field->name;
			$values[] = $field->value;
		}
		$query = "INSERT INTO " . $json_obj->table . " (" . implode(",", $fields) . ") VALUES('" . implode("','", $values) . "')";
		break;

	case "update":
		$changes = [];
		foreach( $json_obj->fields as $field ){
			$changes[] = $field->name . "='" . $field->value . "'";
		}
		$query = "UPDATE " . $json_obj->table . " SET " . implode(",", $changes) . " WHERE id = '" . $json_obj->id . "'";
		break;

	case "delete":
		$query = "DELETE FROM " . $json_obj->table . " WHERE id = '" . $json_obj->id . "'";
		break;	
}

$db_schema = $config['db']['v2']['dbname'];
$db_user = $config['db']['v2']['users']['rw']['username'];
$db_pass = $config['db']['v2']['users']['rw']['password'];
$db_host = $config['db']['v2']['host'];

$conn = new mysqli($db_host,$db_user,$db_pass,$db_schema);
if ($mysqli->connect_errno) {
    echo "Error conectando con BBDD: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

if ($res = $conn->query($query)) {
	echo "Ejecutada consulta: " . $query;
}
else{
	echo "Error ejecutando consulta " . $query . "[" . $conn->error . "]";
}

?>
